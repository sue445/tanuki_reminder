package main

import (
	"gitlab.com/gitlab-org/api/client-go"
	"gitlab.com/sue445/tanuki_reminder/enum"
)

// Notifier represents generic notifier
type Notifier interface {
	NotifyProjectMergeRequests(project *gitlab.Project, mergeRequests []*gitlab.BasicMergeRequest, approvals enum.Approvals) error
	NotifyGroupMergeRequests(group *gitlab.Group, mergeRequests []*gitlab.BasicMergeRequest, projects map[int]*gitlab.Project, approvals enum.Approvals) error
}
