package main

import (
	"fmt"
	"github.com/cockroachdb/errors"
	chatwork "github.com/griffin-stewie/go-chatwork"
	"gitlab.com/gitlab-org/api/client-go"
	"gitlab.com/sue445/tanuki_reminder/enum"
	"strconv"
	"strings"
)

// ChatWorkNotifier represents ChatWork notifier
type ChatWorkNotifier struct {
	APIKey   string
	RoomID   int
	UserList UserList
}

// NewChatWorkNotifier create new ChatWorkNotifier instance
func NewChatWorkNotifier(apiKey string, roomID int, userList UserList) *ChatWorkNotifier {
	return &ChatWorkNotifier{APIKey: apiKey, RoomID: roomID, UserList: userList}
}

// NotifyProjectMergeRequests notifies project merge requests
func (n *ChatWorkNotifier) NotifyProjectMergeRequests(project *gitlab.Project, mergeRequests []*gitlab.BasicMergeRequest, approvals enum.Approvals) error {
	client := chatwork.NewClient(n.APIKey)

	message := n.generateProjectMergeRequestMessage(project, mergeRequests, approvals)
	_, err := client.PostRoomMessage(strconv.Itoa(n.RoomID), message)
	return errors.WithStack(err)
}

func (n *ChatWorkNotifier) generateProjectMergeRequestMessage(project *gitlab.Project, mergeRequests []*gitlab.BasicMergeRequest, approvals enum.Approvals) string {
	if len(mergeRequests) == 0 {
		return fmt.Sprintf("[info][title]No opened MergeRequests at %s[/title][/info]", project.WebURL)
	}

	message := n.generateHeader(project.WebURL, approvals)

	var mergeRequestMessages []string
	for _, mergeRequest := range mergeRequests {
		mergeRequestMessages = append(mergeRequestMessages, n.formatMergeRequest(project, mergeRequest))
	}

	return message + strings.Join(mergeRequestMessages, "[hr]") + "[/info]"
}

// NotifyGroupMergeRequests notifies group merge requests
func (n *ChatWorkNotifier) NotifyGroupMergeRequests(group *gitlab.Group, mergeRequests []*gitlab.BasicMergeRequest, projects map[int]*gitlab.Project, approvals enum.Approvals) error {
	client := chatwork.NewClient(n.APIKey)

	message := n.generateGroupMergeRequestMessage(group, mergeRequests, projects, approvals)
	_, err := client.PostRoomMessage(strconv.Itoa(n.RoomID), message)
	return errors.WithStack(err)
}

func (n *ChatWorkNotifier) generateGroupMergeRequestMessage(group *gitlab.Group, mergeRequests []*gitlab.BasicMergeRequest, projects map[int]*gitlab.Project, approvals enum.Approvals) string {
	if len(mergeRequests) == 0 {
		return fmt.Sprintf("[info][title]No opened MergeRequests at %s[/title][/info]", group.WebURL)
	}

	message := n.generateHeader(group.WebURL, approvals)

	var mergeRequestMessages []string
	for _, mergeRequest := range mergeRequests {
		project := projects[mergeRequest.ProjectID]
		mergeRequestMessages = append(mergeRequestMessages, n.formatMergeRequest(project, mergeRequest))
	}

	return message + strings.Join(mergeRequestMessages, "[hr]") + "[/info]"
}

func (n *ChatWorkNotifier) formatMergeRequest(_ *gitlab.Project, mergeRequest *gitlab.BasicMergeRequest) string {
	message := fmt.Sprintf("%s (%s)\n%s", mergeRequest.Title, mergeRequest.Author.Username, mergeRequest.WebURL)

	if len(mergeRequest.Assignees) > 0 {
		var userNames []string
		for _, assignee := range mergeRequest.Assignees {
			userNames = append(userNames, n.toMention(assignee.Username))
		}

		message += "\nAssigned to "
		message += strings.Join(userNames, ", ")
	}

	if len(mergeRequest.Reviewers) > 0 {
		var userNames []string
		for _, reviewer := range mergeRequest.Reviewers {
			userNames = append(userNames, n.toMention(reviewer.Username))
		}

		message += "\nReviewed to "
		message += strings.Join(userNames, ", ")
	}

	return message
}

func (n *ChatWorkNotifier) toMention(userName string) string {
	accountID := n.UserList[userName]

	if accountID == "" {
		return userName
	}

	return fmt.Sprintf("[To:%s]%s", accountID, userName)
}

func (n *ChatWorkNotifier) generateHeader(url string, approvals enum.Approvals) string {
	message := fmt.Sprintf("[info][title]Opened MergeRequests at %s", url)

	switch approvals {
	case enum.OnlyApproved():
		message += " (Show only accepted MRs)"
	case enum.ExceptApproved():
		message += " (Show only unaccepted MRs)"
	}

	return message + "[/title]"
}
