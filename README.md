# tanuki_reminder
[GitLab™](https://about.gitlab.com/) MergeRequest reminder.

_GITLAB is a trademark of GitLab Inc. in the United States and other countries and regions_

This is clone of [Pull Reminders](https://pullreminders.com/) for GitLab.

![example](img/example.png)

<details>
  <summary>TOC</summary>

[[_TOC_]]

</details>

## Requirements
* GitLab v14.0+ (recommended)
  * see. https://gitlab.com/sue445/tanuki_reminder/-/merge_requests/227
* GitLab API v4
* GitLab Access Token
  * [Personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) (for 2+ projects) or [Project access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) (for 1 project)
  * Role: Developer
  * Scopes: `read_api` (recommended) or `api`

## Supports
* [Slack](https://slack.com/)
* [ChatWork](https://go.chatwork.com/)

## Usage 1. Run on GitLab CI for single project
### 1. Put job setting to `.gitlab-ci.yml`
Slack example

```yaml
include:
  - remote: https://gitlab.com/sue445/tanuki_reminder/raw/master/ci-templates/slack_reminder.yml

slack_reminder:
  variables:
    # GITLAB_ACCESS_TOKEN: ""        # [required]
    # INCLUDE_WIP: "false"           # [optional] put `true` or `false`
    # MERGE_REQUEST_COUNT: 10        # [optional]
    # MERGE_REQUEST_APPROVALS: "all" # [optional] one of 'all', 'only-approved' or 'except-approved'
    # SLACK_WEBHOOK_URL: ""          # [required]
    # SLACK_CHANNEL: ""              # [optional]

  only:
    variables:
      - $SCHEDULER == "reminder"

test:
  script:
    - make test

  # Skip job when scheduler is triggered
  except:
    variables:
      - $SCHEDULER
```

ChatWork example

```yaml
include:
  - remote: https://gitlab.com/sue445/tanuki_reminder/raw/master/ci-templates/chatwork_reminder.yml

chatwork_reminder:
  variables:
    # GITLAB_ACCESS_TOKEN: ""        # [required]
    # INCLUDE_WIP: "false"           # [optional] put `true` or `false`
    # MERGE_REQUEST_COUNT: 10        # [optional]
    # MERGE_REQUEST_APPROVALS: "all" # [optional] one of 'all', 'only-approved' or 'except-approved'
    # CHATWORK_API_KEY: ""           # [required]
    # CHATWORK_ROOM_ID: ""           # [required]

  only:
    variables:
      - $SCHEDULER == "reminder"

test:
  script:
    - make test

  # Skip job when scheduler is triggered
  except:
    variables:
      - $SCHEDULER
```


### 2. Register pipeline schedule
![single project schedule register](img/single-project-schedule-register.png)

### 3. Put variables
Set the following variables with [`variables` in `.gitlab-ci.yml`, `Settings > CI/CD`](https://docs.gitlab.com/ee/ci/variables/#creating-a-custom-environment-variable) or [schedule variables](https://docs.gitlab.com/ee/user/project/pipelines/schedules.html#using-variables)

* `GITLAB_ACCESS_TOKEN`
* `INCLUDE_WIP`
* `MERGE_REQUEST_COUNT`
* `MERGE_REQUEST_APPROVALS`
* `SLACK_WEBHOOK_URL` (only Slack)
* `SLACK_CHANNEL` (only Slack)
  * e.g. `general` (Channel name), `C01234567` (Channel ID)
* `CHATWORK_API_KEY` (only ChatWork)
* `CHATWORK_ROOM_ID` (only ChatWork)
* `SCHEDULER` : some unique variable (e.g. `reminder`)
  * Put to schedule variables

![single project schedule variables](img/single-project-schedule-variables.png)

## Usage 2. Run on GitLab CI for multiple project
### 1. Put job setting to `.gitlab-ci.yml`
Slack example

```yaml
include:
  - remote: https://gitlab.com/sue445/tanuki_reminder/raw/master/ci-templates/slack_reminder.yml

slack_reminder:
  variables:
    # GITLAB_ACCESS_TOKEN: ""        # [required]
    # INCLUDE_WIP: "false"           # [optional] put `true` or `false`
    # MERGE_REQUEST_COUNT: 10        # [optional]
    # MERGE_REQUEST_APPROVALS: "all" # [optional] one of 'all', 'only-approved' or 'except-approved'
    # SLACK_WEBHOOK_URL: ""          # [required]
    # SLACK_CHANNEL: ""              # [optional]
```

ChatWork example

```yaml
include:
  - remote: https://gitlab.com/sue445/tanuki_reminder/raw/master/ci-templates/chatwork_reminder.yml

chatwork_reminder:
  variables:
    # GITLAB_ACCESS_TOKEN: ""        # [required]
    # INCLUDE_WIP: "false"           # [optional] put `true` or `false`
    # MERGE_REQUEST_COUNT: 10        # [optional]
    # MERGE_REQUEST_APPROVALS: "all" # [optional] one of 'all', 'only-approved' or 'except-approved'
    # CHATWORK_API_KEY: ""           # [required]
    # CHATWORK_ROOM_ID: ""           # [required]
```

### 2. Register pipeline schedule
![multiple project schedule register](img/multiple-project-schedule-register.png)

### 3. Put variables
Set the following variables with [`variables` in `.gitlab-ci.yml`, `Settings > CI/CD`](https://docs.gitlab.com/ee/ci/variables/#creating-a-custom-environment-variable) or [schedule variables](https://docs.gitlab.com/ee/user/project/pipelines/schedules.html#using-variables)

* `GITLAB_ACCESS_TOKEN`
* `INCLUDE_WIP`
* `MERGE_REQUEST_COUNT`
* `MERGE_REQUEST_APPROVALS`
* `SLACK_WEBHOOK_URL` (only Slack)
* `SLACK_CHANNEL` (only Slack)
* `CHATWORK_API_KEY` (only ChatWork)
* `CHATWORK_ROOM_ID` (only ChatWork)
* `GITLAB_PROJECT` : repository path (e.g. `gitlab-org/gitlab-foss`)
  * Put to schedule variables

![multiple project schedule variables](img/multiple-project-schedule-variables.png)

## Usage 3. Run on GitLab CI for single group
### 1. Put job setting to `.gitlab-ci.yml`
Slack example

```yaml
include:
  - remote: https://gitlab.com/sue445/tanuki_reminder/raw/master/ci-templates/slack_reminder.yml

slack_reminder:
  variables:
    # GITLAB_ACCESS_TOKEN: ""        # [required]
    # GITLAB_GROUP: ""               # [required if group merge request reminder]
    # INCLUDE_WIP: "false"           # [optional] put `true` or `false`
    # MERGE_REQUEST_COUNT: 10        # [optional]
    # MERGE_REQUEST_APPROVALS: "all" # [optional] one of 'all', 'only-approved' or 'except-approved'
    # SLACK_WEBHOOK_URL: ""          # [required]
    # SLACK_CHANNEL: ""              # [optional]
```

ChatWork example

```yaml
include:
  - remote: https://gitlab.com/sue445/tanuki_reminder/raw/master/ci-templates/chatwork_reminder.yml

chatwork_reminder:
  variables:
    # GITLAB_ACCESS_TOKEN: ""        # [required]
    # GITLAB_GROUP: ""               # [required if group merge request reminder]
    # INCLUDE_WIP: "false"           # [optional] put `true` or `false`
    # MERGE_REQUEST_COUNT: 10        # [optional]
    # MERGE_REQUEST_APPROVALS: "all" # [optional] one of 'all', 'only-approved' or 'except-approved'
    # CHATWORK_API_KEY: ""           # [required]
    # CHATWORK_ROOM_ID: ""           # [required]
```

### 2. Register pipeline schedule
![multiple project schedule register](img/multiple-project-schedule-register.png)

### 3. Put variables
Set the following variables with [`variables` in `.gitlab-ci.yml`, `Settings > CI/CD`](https://docs.gitlab.com/ee/ci/variables/#creating-a-custom-environment-variable) or [schedule variables](https://docs.gitlab.com/ee/user/project/pipelines/schedules.html#using-variables)

* `GITLAB_ACCESS_TOKEN`
* `INCLUDE_WIP`
* `MERGE_REQUEST_COUNT`
* `MERGE_REQUEST_APPROVALS`
* `SLACK_WEBHOOK_URL` (only Slack)
* `SLACK_CHANNEL` (only Slack)
* `CHATWORK_API_KEY` (only ChatWork)
* `CHATWORK_ROOM_ID` (only ChatWork)
* `GITLAB_GROUP` : group path (e.g. `gitlab-org`)
  * Put to schedule variables

![multiple project schedule variables](img/multiple-project-schedule-variables.png)

## Usage 4. Run outside of GitLab CI
Download binary from https://gitlab.com/sue445/tanuki_reminder/-/releases and run

## Usage 5. Docker image
e.g.

```bash
docker run --rm -it registry.gitlab.com/sue445/tanuki_reminder sh

# inside container
tanuki_reminder slack
```

## Commands
### `tanuki_reminder slack`
```bash
$ tanuki_reminder slack --help
NAME:
   tanuki_reminder slack - Notify to Slack

USAGE:
   tanuki_reminder slack [command options] [arguments...]

OPTIONS:
   --approvals value                      Squeeze by whether merge request is approved (one of 'all', 'only-approved' or 'except-approved'. default. 'all') [$MERGE_REQUEST_APPROVALS]
   -c value, --slack-channel value        Slack Channel (e.g. general. default is channel in webhook) [$SLACK_CHANNEL]
   --count value                          MergeRequest count (default: 10) [$MERGE_REQUEST_COUNT]
   -e value, --gitlab-api-endpoint value  GitLab API Endpoint (e.g. http://gitlab.com/api/v4) [$GITLAB_API_ENDPOINT, $CI_API_V4_URL]
   -g value, --gitlab-group value         GitLab Group Path (e.g. gitlab-org) [$GITLAB_GROUP]
   -l value, --user-list value            User list file [$USER_LIST]
   -p value, --gitlab-project value       GitLab Project Path (e.g. gitlab-org/gitlab-foss) [$GITLAB_PROJECT, $CI_PROJECT_PATH]
   -t value, --gitlab-access-token value  GitLab access token [$GITLAB_ACCESS_TOKEN]
   -u value, --slack-url value            Slack incoming Webhook URL [$SLACK_WEBHOOK_URL]
   -w, --wip                              Whether include Draft (formerly WIP) MergeRequest (default. false)
```

### `tanuki_reminder chatwork`
```bash
$ tanuki_reminder chatwork --help

NAME:
   tanuki_reminder chatwork - Notify to ChatWork

USAGE:
   tanuki_reminder chatwork [command options] [arguments...]

OPTIONS:
   --approvals value                      Squeeze by whether merge request is approved (one of 'all', 'only-approved' or 'except-approved'. default. 'all') [$MERGE_REQUEST_APPROVALS]
   --count value                          MergeRequest count (default: 10) [$MERGE_REQUEST_COUNT]
   -e value, --gitlab-api-endpoint value  GitLab API Endpoint (e.g. http://gitlab.com/api/v4) [$GITLAB_API_ENDPOINT, $CI_API_V4_URL]
   -g value, --gitlab-group value         GitLab Group Path (e.g. gitlab-org) [$GITLAB_GROUP]
   -k value, --chatwork-api-key value     ChatWork API Key [$CHATWORK_API_KEY]
   -l value, --user-list value            User list file [$USER_LIST]
   -p value, --gitlab-project value       GitLab Project Path (e.g. gitlab-org/gitlab-foss) [$GITLAB_PROJECT, $CI_PROJECT_PATH]
   -r value, --chatwork-room-id value     ChatWork RoomID (default: 0) [$CHATWORK_ROOM_ID]
   -t value, --gitlab-access-token value  GitLab access token [$GITLAB_ACCESS_TOKEN]
   -w, --wip                              Whether include Draft (formerly WIP) MergeRequest (default. false)
```

## ProTips
### user mapping list
Often, GitLab username will not match Slack username.

You can create user list for mapping.

user mapping list is following format

#### for Slack
```yaml
# slack_users.yml
gitlab_username:  slack_username
gitlab_username2: UXXXXXXXX # Slack UserID
```

```bash
/path/to/tanuki_reminder slack --user-list slack_users.yml
```

When MergeRequest is assigned to `@gitlab_username`, Slack mentions to `@slack_username`.

#### for ChatWork
```yaml
# chatwork_users.yml
gitlab_username: 12345 # ChatWork account ID
```

```bash
/path/to/tanuki_reminder chatwork --user-list chatwork_users.yml
```

When MergeRequest is assigned to `@gitlab_username`, ChatWork mentions to `[To:12345]gitlab_username`.

## Note
### Number of pipeline schedules
There is an upper limit on the number of schedules per project.

If you have GitLab installed yourself, you can increase this limit.

c.f. https://docs.gitlab.com/ee/administration/instance_limits.html#number-of-pipeline-schedules

## Maintenance
### Upgrade Go version
Fix followings

* [.gitlab-ci.yml](.gitlab-ci.yml)
* [Dockerfile](Dockerfile)
* [go.mod](go.mod)

And run `make go_mod_tidy`

e.g.

* https://gitlab.com/sue445/tanuki_reminder/-/merge_requests/159
* https://gitlab.com/sue445/tanuki_reminder/-/merge_requests/175

## LICENSE
All programs are licensed under the [MIT License](LICENSE) Copyright (c) 2019 sue445.

But only [repository avatar](https://www.irasutoya.com/2016/03/blog-post_98.html)'s LICENCE belongs to [irasutoya.com](https://www.irasutoya.com/)
