package main

import (
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/api/client-go"
	"gitlab.com/sue445/tanuki_reminder/enum"
	"testing"
)

func TestSlackNotifier_NotifyProjectMergeRequests(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder("POST", "https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX",
		httpmock.NewStringResponder(200, ""))

	type fields struct {
		WebhookURL string
		Channel    string
	}
	type args struct {
		project       *gitlab.Project
		mergeRequests []*gitlab.BasicMergeRequest
		approvals     enum.Approvals
	}

	project := &gitlab.Project{
		PathWithNamespace: "my-group/my-project",
		WebURL:            "http://gitlab.example.com/my-group/my-project",
	}
	var mergeRequests []*gitlab.BasicMergeRequest

	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "With channel",
			fields: fields{
				WebhookURL: "https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX",
				Channel:    "#general",
			},
			args: args{
				project:       project,
				mergeRequests: mergeRequests,
				approvals:     enum.All(),
			},
		},
		{
			name: "Without channel",
			fields: fields{
				WebhookURL: "https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX",
				Channel:    "",
			},
			args: args{
				project:       project,
				mergeRequests: mergeRequests,
				approvals:     enum.All(),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := &SlackNotifier{
				WebhookURL: tt.fields.WebhookURL,
				Channel:    tt.fields.Channel,
			}
			err := n.NotifyProjectMergeRequests(tt.args.project, tt.args.mergeRequests, tt.args.approvals)
			assert.NoError(t, err)
		})
	}
}

func TestSlackNotifier_NotifyGroupMergeRequests(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder("POST", "https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX",
		httpmock.NewStringResponder(200, ""))

	type fields struct {
		WebhookURL string
		Channel    string
	}
	type args struct {
		group         *gitlab.Group
		mergeRequests []*gitlab.BasicMergeRequest
		projects      map[int]*gitlab.Project
		approvals     enum.Approvals
	}

	group := &gitlab.Group{
		FullPath: "my-group",
		WebURL:   "http://gitlab.example.com/my-group",
	}
	var mergeRequests []*gitlab.BasicMergeRequest
	projects := map[int]*gitlab.Project{}

	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "With channel",
			fields: fields{
				WebhookURL: "https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX",
				Channel:    "#general",
			},
			args: args{
				group:         group,
				mergeRequests: mergeRequests,
				projects:      projects,
				approvals:     enum.All(),
			},
		},
		{
			name: "Without channel",
			fields: fields{
				WebhookURL: "https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX",
				Channel:    "",
			},
			args: args{
				group:         group,
				mergeRequests: mergeRequests,
				projects:      projects,
				approvals:     enum.All(),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := &SlackNotifier{
				WebhookURL: tt.fields.WebhookURL,
				Channel:    tt.fields.Channel,
			}
			err := n.NotifyGroupMergeRequests(tt.args.group, tt.args.mergeRequests, tt.args.projects, tt.args.approvals)
			assert.NoError(t, err)
		})
	}
}

func TestSlackNotifier_formatMergeRequest(t *testing.T) {
	type args struct {
		project      *gitlab.Project
		mergeRequest *gitlab.BasicMergeRequest
	}

	project := &gitlab.Project{
		PathWithNamespace: "my-group/my-project",
		WebURL:            "http://gitlab.example.com/my-group/my-project",
	}

	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "MR no assignees and reviewers",
			args: args{
				project: project,
				mergeRequest: &gitlab.BasicMergeRequest{
					Title:  "test MR",
					IID:    1,
					WebURL: "http://gitlab.example.com/my-group/my-project/merge_requests/1",
					Author: &gitlab.BasicUser{
						Username: "author",
					},
					Assignees: []*gitlab.BasicUser{},
					Reviewers: []*gitlab.BasicUser{},
				},
			},
			want: "[my-group/my-project!1] <http://gitlab.example.com/my-group/my-project/merge_requests/1|test MR> (_author_)",
		},
		{
			name: "MR has only assignees",
			args: args{
				project: project,
				mergeRequest: &gitlab.BasicMergeRequest{
					Title:  "test MR",
					IID:    1,
					WebURL: "http://gitlab.example.com/my-group/my-project/merge_requests/1",
					Author: &gitlab.BasicUser{
						Username: "author",
					},
					Assignees: []*gitlab.BasicUser{
						{
							Username: "assignee1",
						},
						{
							Username: "assignee2",
						},
					},
					Reviewers: []*gitlab.BasicUser{},
				},
			},
			want: `[my-group/my-project!1] <http://gitlab.example.com/my-group/my-project/merge_requests/1|test MR> (_author_)
Assigned to <@assignee1>, <@assignee2>`,
		},
		{
			name: "MR has only reviewers",
			args: args{
				project: project,
				mergeRequest: &gitlab.BasicMergeRequest{
					Title:  "test MR",
					IID:    1,
					WebURL: "http://gitlab.example.com/my-group/my-project/merge_requests/1",
					Author: &gitlab.BasicUser{
						Username: "author",
					},
					Assignees: []*gitlab.BasicUser{},
					Reviewers: []*gitlab.BasicUser{
						{
							Username: "reviewer1",
						},
						{
							Username: "reviewer2",
						},
					},
				},
			},
			want: `[my-group/my-project!1] <http://gitlab.example.com/my-group/my-project/merge_requests/1|test MR> (_author_)
Reviewed to <@reviewer1>, <@reviewer2>`,
		},
		{
			name: "MR has only assignees",
			args: args{
				project: project,
				mergeRequest: &gitlab.BasicMergeRequest{
					Title:  "test MR",
					IID:    1,
					WebURL: "http://gitlab.example.com/my-group/my-project/merge_requests/1",
					Author: &gitlab.BasicUser{
						Username: "author",
					},
					Assignees: []*gitlab.BasicUser{
						{
							Username: "assignee1",
						},
						{
							Username: "assignee2",
						},
					},
					Reviewers: []*gitlab.BasicUser{
						{
							Username: "reviewer1",
						},
						{
							Username: "reviewer2",
						},
					},
				},
			},
			want: `[my-group/my-project!1] <http://gitlab.example.com/my-group/my-project/merge_requests/1|test MR> (_author_)
Assigned to <@assignee1>, <@assignee2>
Reviewed to <@reviewer1>, <@reviewer2>`,
		},
	}

	n := &SlackNotifier{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := n.formatMergeRequest(tt.args.project, tt.args.mergeRequest)
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestSlackNotifier_generateProjectMergeRequestMessage(t *testing.T) {
	type args struct {
		project       *gitlab.Project
		mergeRequests []*gitlab.BasicMergeRequest
		approvals     enum.Approvals
	}

	project := &gitlab.Project{
		PathWithNamespace: "my-group/my-project",
		WebURL:            "http://gitlab.example.com/my-group/my-project",
	}
	mergeRequests := []*gitlab.BasicMergeRequest{
		{
			Title:  "MR has assignees and reviewers",
			IID:    1,
			WebURL: "http://gitlab.example.com/my-group/my-project/merge_requests/1",
			Author: &gitlab.BasicUser{
				Username: "author",
			},
			Assignees: []*gitlab.BasicUser{
				{
					Username: "assignee1",
				},
				{
					Username: "assignee2",
				},
			},
			Reviewers: []*gitlab.BasicUser{
				{
					Username: "reviewer1",
				},
				{
					Username: "reviewer2",
				},
			},
		},
		{
			Title:  "MR no assignees and reviewers",
			IID:    2,
			WebURL: "http://gitlab.example.com/my-group/my-project/merge_requests/2",
			Author: &gitlab.BasicUser{
				Username: "author",
			},
		},
	}

	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "Has no MergeRequests",
			args: args{
				project:   project,
				approvals: enum.All(),
			},
			want: "No opened MergeRequests at <http://gitlab.example.com/my-group/my-project|my-group/my-project>",
		},
		{
			name: "Has MergeRequests, All",
			args: args{
				project:       project,
				mergeRequests: mergeRequests,
				approvals:     enum.All(),
			},
			want: `Opened MergeRequests at <http://gitlab.example.com/my-group/my-project|my-group/my-project>

[my-group/my-project!1] <http://gitlab.example.com/my-group/my-project/merge_requests/1|MR has assignees and reviewers> (_author_)
Assigned to <@assignee1>, <@assignee2>
Reviewed to <@reviewer1>, <@reviewer2>

[my-group/my-project!2] <http://gitlab.example.com/my-group/my-project/merge_requests/2|MR no assignees and reviewers> (_author_)`,
		},
		{
			name: "Has MergeRequests, OnlyApproved",
			args: args{
				project:       project,
				mergeRequests: mergeRequests,
				approvals:     enum.OnlyApproved(),
			},
			want: `Opened MergeRequests at <http://gitlab.example.com/my-group/my-project|my-group/my-project> (Show only accepted MRs)

[my-group/my-project!1] <http://gitlab.example.com/my-group/my-project/merge_requests/1|MR has assignees and reviewers> (_author_)
Assigned to <@assignee1>, <@assignee2>
Reviewed to <@reviewer1>, <@reviewer2>

[my-group/my-project!2] <http://gitlab.example.com/my-group/my-project/merge_requests/2|MR no assignees and reviewers> (_author_)`,
		},
		{
			name: "Has MergeRequests, ExceptApproved",
			args: args{
				project:       project,
				mergeRequests: mergeRequests,
				approvals:     enum.ExceptApproved(),
			},
			want: `Opened MergeRequests at <http://gitlab.example.com/my-group/my-project|my-group/my-project> (Show only unaccepted MRs)

[my-group/my-project!1] <http://gitlab.example.com/my-group/my-project/merge_requests/1|MR has assignees and reviewers> (_author_)
Assigned to <@assignee1>, <@assignee2>
Reviewed to <@reviewer1>, <@reviewer2>

[my-group/my-project!2] <http://gitlab.example.com/my-group/my-project/merge_requests/2|MR no assignees and reviewers> (_author_)`,
		},
	}

	n := &SlackNotifier{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := n.generateProjectMergeRequestMessage(tt.args.project, tt.args.mergeRequests, tt.args.approvals)
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestSlackNotifier_generateGroupMergeRequestMessage(t *testing.T) {
	type args struct {
		group         *gitlab.Group
		mergeRequests []*gitlab.BasicMergeRequest
		projects      map[int]*gitlab.Project
		approvals     enum.Approvals
	}

	group := &gitlab.Group{
		FullPath: "my-group",
		WebURL:   "http://gitlab.example.com/my-group",
	}
	mergeRequests := []*gitlab.BasicMergeRequest{
		{
			Title:     "MR has assignees and reviewers",
			IID:       1,
			ProjectID: 1,
			WebURL:    "http://gitlab.example.com/my-group/my-project/merge_requests/1",
			Author: &gitlab.BasicUser{
				Username: "author",
			},
			Assignees: []*gitlab.BasicUser{
				{
					Username: "assignee1",
				},
				{
					Username: "assignee2",
				},
			},
			Reviewers: []*gitlab.BasicUser{
				{
					Username: "reviewer1",
				},
				{
					Username: "reviewer2",
				},
			},
		},
		{
			Title:     "MR no assignees and reviewers",
			IID:       2,
			ProjectID: 1,
			WebURL:    "http://gitlab.example.com/my-group/my-project/merge_requests/2",
			Author: &gitlab.BasicUser{
				Username: "author",
			},
		},
	}
	projects := map[int]*gitlab.Project{
		1: {
			PathWithNamespace: "my-group/my-project",
			WebURL:            "http://gitlab.example.com/my-group/my-project",
		},
	}

	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "Has no MergeRequests",
			args: args{
				group:     group,
				projects:  projects,
				approvals: enum.All(),
			},
			want: "No opened MergeRequests at <http://gitlab.example.com/my-group|my-group>",
		},
		{
			name: "Has MergeRequests, All",
			args: args{
				group:         group,
				projects:      projects,
				mergeRequests: mergeRequests,
				approvals:     enum.All(),
			},
			want: `Opened MergeRequests at <http://gitlab.example.com/my-group|my-group>

[my-group/my-project!1] <http://gitlab.example.com/my-group/my-project/merge_requests/1|MR has assignees and reviewers> (_author_)
Assigned to <@assignee1>, <@assignee2>
Reviewed to <@reviewer1>, <@reviewer2>

[my-group/my-project!2] <http://gitlab.example.com/my-group/my-project/merge_requests/2|MR no assignees and reviewers> (_author_)`,
		},
		{
			name: "Has MergeRequests, OnlyApproved",
			args: args{
				group:         group,
				projects:      projects,
				mergeRequests: mergeRequests,
				approvals:     enum.OnlyApproved(),
			},
			want: `Opened MergeRequests at <http://gitlab.example.com/my-group|my-group> (Show only accepted MRs)

[my-group/my-project!1] <http://gitlab.example.com/my-group/my-project/merge_requests/1|MR has assignees and reviewers> (_author_)
Assigned to <@assignee1>, <@assignee2>
Reviewed to <@reviewer1>, <@reviewer2>

[my-group/my-project!2] <http://gitlab.example.com/my-group/my-project/merge_requests/2|MR no assignees and reviewers> (_author_)`,
		},
		{
			name: "Has MergeRequests, ExceptApproved",
			args: args{
				group:         group,
				projects:      projects,
				mergeRequests: mergeRequests,
				approvals:     enum.ExceptApproved(),
			},
			want: `Opened MergeRequests at <http://gitlab.example.com/my-group|my-group> (Show only unaccepted MRs)

[my-group/my-project!1] <http://gitlab.example.com/my-group/my-project/merge_requests/1|MR has assignees and reviewers> (_author_)
Assigned to <@assignee1>, <@assignee2>
Reviewed to <@reviewer1>, <@reviewer2>

[my-group/my-project!2] <http://gitlab.example.com/my-group/my-project/merge_requests/2|MR no assignees and reviewers> (_author_)`,
		},
	}

	n := &SlackNotifier{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := n.generateGroupMergeRequestMessage(tt.args.group, tt.args.mergeRequests, tt.args.projects, tt.args.approvals)
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestSlackNotifier_toMention(t *testing.T) {
	type fields struct {
		WebhookURL string
		Channel    string
		UserList   UserList
	}
	type args struct {
		userName string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "not included in UserList",
			fields: fields{
				UserList: UserList{
					"gitlab_user": "slack_user",
				},
			},
			args: args{
				userName: "root",
			},
			want: "<@root>",
		},
		{
			name: "included in UserList",
			fields: fields{
				UserList: UserList{
					"gitlab_user": "slack_user",
				},
			},
			args: args{
				userName: "gitlab_user",
			},
			want: "<@slack_user>",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := &SlackNotifier{
				WebhookURL: tt.fields.WebhookURL,
				Channel:    tt.fields.Channel,
				UserList:   tt.fields.UserList,
			}
			got := n.toMention(tt.args.userName)
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestSlackNotifier_sanitizeText(t *testing.T) {
	type args struct {
		str string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "normal",
			args: args{
				str: "abc",
			},
			want: "abc",
		},
		{
			name: "escaping text",
			args: args{
				str: "<abc> & <def>",
			},
			want: "&lt;abc&gt; &amp; &lt;def&gt;",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := &SlackNotifier{}
			got := n.sanitizeText(tt.args.str)
			assert.Equal(t, tt.want, got)
		})
	}
}
