FROM golang:1.24-alpine AS build-env

ADD . /work
WORKDIR /work

RUN apk --update add make git \
 && make

FROM alpine
COPY --from=build-env /work/bin/tanuki_reminder /usr/local/bin/tanuki_reminder
