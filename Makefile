# Requirements: git, go, vgo
NAME     := tanuki_reminder
VERSION  := $(shell cat VERSION)
REVISION := $(shell git rev-parse --short HEAD)

SRCS    := $(shell find . -type f -name '*.go')
LDFLAGS := "-s -w -X \"main.Version=$(VERSION)\" -X \"main.Revision=$(REVISION)\" -extldflags \"-static\""

.DEFAULT_GOAL := bin/$(NAME)

export GO111MODULE ?= on

bin/$(NAME): $(SRCS)
	go build -ldflags=$(LDFLAGS) -o bin/$(NAME)

.PHONY: gox
gox:
	gox -ldflags=$(LDFLAGS) -osarch "!darwin/386" -output="bin/$(NAME)_{{.OS}}_{{.Arch}}"

.PHONY: zip
zip:
	cd bin ; \
	for file in *; do \
		zip $${file}.zip $${file} ; \
	done

.PHONY: gox_with_zip
gox_with_zip: clean gox zip

.PHONY: clean
clean:
	rm -rf bin/*

.PHONY: tag
tag: check_version
	git tag -a $(VERSION) -m "Release $(VERSION)"
	git push --tags

.PHONY: check_version
check_version:
	go run _scripts/template_version_checker.go

.PHONY: release
release: tag
	git push origin master

.PHONY: test
test:
	go test -count=1 $${TEST_ARGS} ./...

.PHONY: testrace
testrace:
	go test -count=1 $${TEST_ARGS} -race ./...

.PHONY: fmt
fmt:
	go fmt ./...

.PHONY: test_all
test_all: test testrace fmt

.PHONY: go_mod_tidy
go_mod_tidy:
	go mod tidy
	git add go.*
	git commit -m "go mod tidy" || true
