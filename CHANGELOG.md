## Unreleased
[full changelog](https://gitlab.com/sue445/tanuki_reminder/compare/0.6.10...master)

## [0.6.10](https://gitlab.com/sue445/tanuki_reminder/-/releases/0.6.10)
[full changelog](https://gitlab.com/sue445/tanuki_reminder/compare/0.6.9...0.6.10)

* Tweak `--wip` description [!228](https://gitlab.com/sue445/tanuki_reminder/-/merge_requests/228) *@sue445*
* doc: Now, GitLab v14.0+ is recommended [!227](https://gitlab.com/sue445/tanuki_reminder/-/merge_requests/227) *@sue445*
* Upgrade dependencies

## [0.6.9](https://gitlab.com/sue445/tanuki_reminder/-/releases/0.6.9)
[full changelog](https://gitlab.com/sue445/tanuki_reminder/compare/0.6.8...0.6.9)

* Fix go version in Dockerfile [!222](https://gitlab.com/sue445/tanuki_reminder/-/merge_requests/222) *@sue445*

## [0.6.8](https://gitlab.com/sue445/tanuki_reminder/-/releases/0.6.8)
[full changelog](https://gitlab.com/sue445/tanuki_reminder/compare/0.6.7...0.6.8)

* Upgrade to Go 1.24 [!221](https://gitlab.com/sue445/tanuki_reminder/merge_requests/221) *@sue445*
* Migrate to gitlab.com/gitlab-org/api/client-go [!211](https://gitlab.com/sue445/tanuki_reminder/-/merge_requests/211) *@sue445*
* Upgrade dependencies

## 0.6.7
[full changelog](https://gitlab.com/sue445/tanuki_reminder/compare/0.6.6...0.6.7)

* Upgrade to Go 1.23 [!198](https://gitlab.com/sue445/tanuki_reminder/merge_requests/198) *@sue445*
* Upgrade dependencies

## 0.6.6
[full changelog](https://gitlab.com/sue445/tanuki_reminder/compare/0.6.5...0.6.6)

* Add golangci-lint [!193](https://gitlab.com/sue445/tanuki_reminder/merge_requests/193) *@sue445*

## 0.6.5
[full changelog](https://gitlab.com/sue445/tanuki_reminder/compare/0.6.4...0.6.5)

* [Security] Bump github.com/hashicorp/go-retryablehttp from 0.7.2 to 0.7.7 [!188](https://gitlab.com/sue445/tanuki_reminder/merge_requests/188) *@sue445*
* Upgrade dependencies

## 0.6.4
[full changelog](https://gitlab.com/sue445/tanuki_reminder/compare/0.6.3...0.6.4)

* [Security][CVE-2024-24786] Bump google.golang.org/protobuf from 1.29.1 to 1.33.0 [!179](https://gitlab.com/sue445/tanuki_reminder/merge_requests/179) *@sue445*
* Upgrade to Go 1.22 [!175](https://gitlab.com/sue445/tanuki_reminder/merge_requests/175) *@sue445*
* Upgrade dependencies

## 0.6.3
[full changelog](https://gitlab.com/sue445/tanuki_reminder/compare/0.6.2...0.6.3)

* Migrate to github.com/slack-go/slack [!171](https://gitlab.com/sue445/tanuki_reminder/merge_requests/171) *@sue445*
* Upgrade dependencies

## 0.6.2
[full changelog](https://gitlab.com/sue445/tanuki_reminder/compare/0.6.1...0.6.2)

* Upgrade to Go 1.21 [!159](https://gitlab.com/sue445/tanuki_reminder/merge_requests/159) *@sue445*
* Upgrade dependencies

## 0.6.1
[full changelog](https://gitlab.com/sue445/tanuki_reminder/compare/0.6.0...0.6.1)

* Upgrade to Go 1.20 [!125](https://gitlab.com/sue445/tanuki_reminder/merge_requests/125) *@sue445*
* Wrap all errors with `errors.WithStack` [!145](https://gitlab.com/sue445/tanuki_reminder/merge_requests/145) *@sue445*
* Fixed not returning an error when not found projects [!146](https://gitlab.com/sue445/tanuki_reminder/merge_requests/146) *@sue445*
* Upgrade dependencies

## 0.6.0
[full changelog](https://gitlab.com/sue445/tanuki_reminder/compare/0.5.0...0.6.0)

### Features
* Support reviewers [!73](https://gitlab.com/sue445/tanuki_reminder/merge_requests/73) *@sue445*

### Others
* Upload coverage report to GitLab [!60](https://gitlab.com/sue445/tanuki_reminder/merge_requests/60) *@sue445*
* Deploy coverage report to GitLab Pages [!61](https://gitlab.com/sue445/tanuki_reminder/merge_requests/61) *@sue445*
* Add repository avatar's LICENSE [!76](https://gitlab.com/sue445/tanuki_reminder/merge_requests/76) *@sue445*
* Upgrade dependencies

## 0.5.0
[full changelog](https://gitlab.com/sue445/tanuki_reminder/compare/0.4.0...0.5.0)

* Add `--approvals` option [!59](https://gitlab.com/sue445/tanuki_reminder/merge_requests/59) *@sue445*

## 0.4.0
[full changelog](https://gitlab.com/sue445/tanuki_reminder/compare/0.3.0...0.4.0)

### Features
* Add group merge request notification [!56](https://gitlab.com/sue445/tanuki_reminder/merge_requests/56) *@sue445*

### Bugfixes
* Bugfixed. slack message is broken when `<`, `>` or `&` contains in MR title [!58](https://gitlab.com/sue445/tanuki_reminder/-/merge_requests/58) *@sue445*

### Others
* Upgrade to go 1.15 [!55](https://gitlab.com/sue445/tanuki_reminder/merge_requests/55) *@sue445*
* Upgrade dependencies

## 0.3.0
[full changelog](https://gitlab.com/sue445/tanuki_reminder/compare/0.2.1...0.3.0)

### Features
* Add `--count` option [!27](https://gitlab.com/sue445/tanuki_reminder/merge_requests/27) *@sue445*

### Others
* Refactor: use IntFlag instead of StringFlag [!28](https://gitlab.com/sue445/tanuki_reminder/merge_requests/28) *@sue445*

## 0.2.1
[full changelog](https://gitlab.com/sue445/tanuki_reminder/compare/0.2.0...0.2.1)

### Bugfix
* Fix job name in template [!26](https://gitlab.com/sue445/tanuki_reminder/merge_requests/26) *@sue445*

## 0.2.0
[full changelog](https://gitlab.com/sue445/tanuki_reminder/compare/0.1.1...0.2.0)

### Features
* Support ChatWork [!25](https://gitlab.com/sue445/tanuki_reminder/merge_requests/25) *@sue445*

### Others
* Fixed. Don't run dependabot job when commit pushing [!23](https://gitlab.com/sue445/tanuki_reminder/merge_requests/23) *@sue445*
* Refactor: CI setting [!24](https://gitlab.com/sue445/tanuki_reminder/merge_requests/24) *@sue445*
* Write doc [!17](https://gitlab.com/sue445/tanuki_reminder/merge_requests/17) *@sue445*

## 0.1.1
[full changelog](https://gitlab.com/sue445/tanuki_reminder/compare/0.1.0...0.1.1)

* Setup dependabot-script [!18](https://gitlab.com/sue445/tanuki_reminder/merge_requests/18) *@sue445*
* Bump gopkg.in/yaml.v2 from 2.2.2 to 2.2.5 [!20](https://gitlab.com/sue445/tanuki_reminder/merge_requests/20) *@sue445*
* Bump github.com/xanzy/go-gitlab from 0.21.0 to 0.22.0 [!19](https://gitlab.com/sue445/tanuki_reminder/merge_requests/19) *@sue445*
* GITLAB_PRIVATE_TOKEN -> GITLAB_ACCESS_TOKEN [!21](https://gitlab.com/sue445/tanuki_reminder/merge_requests/21) *@sue445*

## 0.1.0
* first release
