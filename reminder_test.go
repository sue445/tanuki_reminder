package main

import (
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/api/client-go"
	"gitlab.com/sue445/tanuki_reminder/enum"
	"net/http"
	"testing"
)

type mockNotifier struct {
	notifyProjectMergeRequestsCalledCount int
	notifyGroupMergeRequestsCalledCount   int
}

func (n *mockNotifier) NotifyProjectMergeRequests(_ *gitlab.Project, _ []*gitlab.BasicMergeRequest, _ enum.Approvals) error {
	n.notifyProjectMergeRequestsCalledCount++
	return nil
}

func (n *mockNotifier) NotifyGroupMergeRequests(_ *gitlab.Group, _ []*gitlab.BasicMergeRequest, _ map[int]*gitlab.Project, _ enum.Approvals) error {
	n.notifyGroupMergeRequestsCalledCount++
	return nil
}

func TestReminder_Run(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder(
		"GET",
		"http://example.com/api/v4/",
		func(_ *http.Request) (*http.Response, error) {
			resp := httpmock.NewStringResponse(404, "{\"error\":\"404 Not Found\"}")
			resp.Header.Set("RateLimit-Limit", "600")
			return resp, nil
		},
	)
	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/projects/my-group%2Fmy-project/merge_requests?page=1&per_page=100&state=opened",
		httpmock.NewStringResponder(200, ReadTestData("testdata/gitlab/list_project_merge_requests_with_wip.json")))
	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/projects/my-group%2Fmy-project",
		httpmock.NewStringResponder(200, ReadTestData("testdata/gitlab/get_single_project.json")))
	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/groups/my-group",
		httpmock.NewStringResponder(200, ReadTestData("testdata/gitlab/get_single_group.json")))
	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/groups/my-group/merge_requests?page=1&per_page=100&state=opened",
		httpmock.NewStringResponder(200, ReadTestData("testdata/gitlab/list_group_merge_requests.json")))
	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/projects/3",
		httpmock.NewStringResponder(200, ReadTestData("testdata/gitlab/get_single_project.json")))

	type fields struct {
		GitLabClientParams *GitLabClientParams
		GitLabProject      string
		GitLabGroup        string
		IncludeWip         bool
		MergeRequestCount  int
		Approvals          enum.Approvals
	}
	type wants struct {
		notifyProjectMergeRequestsCalledCount int
		notifyGroupMergeRequestsCalledCount   int
	}

	httpClient := http.DefaultClient

	tests := []struct {
		name   string
		fields fields
		wants  wants
	}{
		{
			name: "when notifyProjectMergeRequests is called",
			fields: fields{
				GitLabClientParams: &GitLabClientParams{
					APIEndpoint:  "https://gitlab.example.com/api/v4",
					PrivateToken: "XXXXXX",
					HTTPClient:   httpClient,
				},
				GitLabProject:     "my-group/my-project",
				GitLabGroup:       "",
				IncludeWip:        true,
				MergeRequestCount: 100,
				Approvals:         enum.All(),
			},
			wants: wants{
				notifyProjectMergeRequestsCalledCount: 1,
				notifyGroupMergeRequestsCalledCount:   0,
			},
		},
		{
			name: "when notifyGroupMergeRequests is called",
			fields: fields{
				GitLabClientParams: &GitLabClientParams{
					APIEndpoint:  "https://gitlab.example.com/api/v4",
					PrivateToken: "XXXXXX",
					HTTPClient:   httpClient,
				},
				GitLabProject:     "",
				GitLabGroup:       "my-group",
				IncludeWip:        true,
				MergeRequestCount: 100,
				Approvals:         enum.All(),
			},
			wants: wants{
				notifyProjectMergeRequestsCalledCount: 0,
				notifyGroupMergeRequestsCalledCount:   1,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Reminder{
				GitLabClientParams: tt.fields.GitLabClientParams,
				GitLabProject:      tt.fields.GitLabProject,
				GitLabGroup:        tt.fields.GitLabGroup,
				includeWip:         tt.fields.IncludeWip,
				MergeRequestCount:  tt.fields.MergeRequestCount,
				Approvals:          tt.fields.Approvals,
			}

			mockNotifier := &mockNotifier{}
			err := r.Run(mockNotifier)

			if assert.NoError(t, err) {
				assert.Equal(t, tt.wants.notifyProjectMergeRequestsCalledCount, mockNotifier.notifyProjectMergeRequestsCalledCount)
				assert.Equal(t, tt.wants.notifyGroupMergeRequestsCalledCount, mockNotifier.notifyGroupMergeRequestsCalledCount)
			}
		})
	}
}
