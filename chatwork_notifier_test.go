package main

import (
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/api/client-go"
	"gitlab.com/sue445/tanuki_reminder/enum"
	"testing"
)

func TestChatWorkNotifier_NotifyProjectMergeRequests(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder("POST", "https://api.chatwork.com/v2/rooms/12345/messages",
		httpmock.NewStringResponder(200, ""))

	type fields struct {
		APIKey   string
		RoomID   int
		UserList UserList
	}
	type args struct {
		project       *gitlab.Project
		mergeRequests []*gitlab.BasicMergeRequest
		approvals     enum.Approvals
	}

	project := &gitlab.Project{
		PathWithNamespace: "my-group/my-project",
		WebURL:            "http://gitlab.example.com/my-group/my-project",
	}
	var mergeRequests []*gitlab.BasicMergeRequest

	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "Successful",
			fields: fields{
				APIKey: "XXXXX",
				RoomID: 12345,
			},
			args: args{
				project:       project,
				mergeRequests: mergeRequests,
				approvals:     enum.All(),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := &ChatWorkNotifier{
				APIKey:   tt.fields.APIKey,
				RoomID:   tt.fields.RoomID,
				UserList: tt.fields.UserList,
			}
			err := n.NotifyProjectMergeRequests(tt.args.project, tt.args.mergeRequests, tt.args.approvals)
			assert.NoError(t, err)
		})
	}
}

func TestChatWorkNotifier_generateProjectMergeRequestMessage(t *testing.T) {
	type fields struct {
		APIKey   string
		RoomID   int
		UserList UserList
	}
	type args struct {
		project       *gitlab.Project
		mergeRequests []*gitlab.BasicMergeRequest
		approvals     enum.Approvals
	}

	project := &gitlab.Project{
		PathWithNamespace: "my-group/my-project",
		WebURL:            "http://gitlab.example.com/my-group/my-project",
	}
	mergeRequests := []*gitlab.BasicMergeRequest{
		{
			Title:  "MR has assignees and reviewers",
			IID:    1,
			WebURL: "http://gitlab.example.com/my-group/my-project/merge_requests/1",
			Author: &gitlab.BasicUser{
				Username: "author",
			},
			Assignees: []*gitlab.BasicUser{
				{
					Username: "assignee1",
				},
				{
					Username: "assignee2",
				},
			},
			Reviewers: []*gitlab.BasicUser{
				{
					Username: "reviewer1",
				},
				{
					Username: "reviewer2",
				},
			},
		},
		{
			Title:  "MR no assignees and reviewers",
			IID:    2,
			WebURL: "http://gitlab.example.com/my-group/my-project/merge_requests/2",
			Author: &gitlab.BasicUser{
				Username: "author",
			},
		},
	}

	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Has no MergeRequests",
			args: args{
				project:   project,
				approvals: enum.All(),
			},
			want: "[info][title]No opened MergeRequests at http://gitlab.example.com/my-group/my-project[/title][/info]",
		},
		{
			name: "Has MergeRequests, All",
			args: args{
				project:       project,
				mergeRequests: mergeRequests,
				approvals:     enum.All(),
			},
			want: `[info][title]Opened MergeRequests at http://gitlab.example.com/my-group/my-project[/title]MR has assignees and reviewers (author)
http://gitlab.example.com/my-group/my-project/merge_requests/1
Assigned to assignee1, assignee2
Reviewed to reviewer1, reviewer2[hr]MR no assignees and reviewers (author)
http://gitlab.example.com/my-group/my-project/merge_requests/2[/info]`,
		},
		{
			name: "Has MergeRequests, OnlyApproved",
			args: args{
				project:       project,
				mergeRequests: mergeRequests,
				approvals:     enum.OnlyApproved(),
			},
			want: `[info][title]Opened MergeRequests at http://gitlab.example.com/my-group/my-project (Show only accepted MRs)[/title]MR has assignees and reviewers (author)
http://gitlab.example.com/my-group/my-project/merge_requests/1
Assigned to assignee1, assignee2
Reviewed to reviewer1, reviewer2[hr]MR no assignees and reviewers (author)
http://gitlab.example.com/my-group/my-project/merge_requests/2[/info]`,
		},
		{
			name: "Has MergeRequests, ExceptApproved",
			args: args{
				project:       project,
				mergeRequests: mergeRequests,
				approvals:     enum.ExceptApproved(),
			},
			want: `[info][title]Opened MergeRequests at http://gitlab.example.com/my-group/my-project (Show only unaccepted MRs)[/title]MR has assignees and reviewers (author)
http://gitlab.example.com/my-group/my-project/merge_requests/1
Assigned to assignee1, assignee2
Reviewed to reviewer1, reviewer2[hr]MR no assignees and reviewers (author)
http://gitlab.example.com/my-group/my-project/merge_requests/2[/info]`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := &ChatWorkNotifier{
				APIKey:   tt.fields.APIKey,
				RoomID:   tt.fields.RoomID,
				UserList: tt.fields.UserList,
			}
			got := n.generateProjectMergeRequestMessage(tt.args.project, tt.args.mergeRequests, tt.args.approvals)
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestChatWorkNotifier_NotifyGroupMergeRequests(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder("POST", "https://api.chatwork.com/v2/rooms/12345/messages",
		httpmock.NewStringResponder(200, ""))

	type fields struct {
		APIKey   string
		RoomID   int
		UserList UserList
	}
	type args struct {
		group         *gitlab.Group
		mergeRequests []*gitlab.BasicMergeRequest
		projects      map[int]*gitlab.Project
		approvals     enum.Approvals
	}

	group := &gitlab.Group{
		FullPath: "my-group",
		WebURL:   "http://gitlab.example.com/my-group",
	}
	var mergeRequests []*gitlab.BasicMergeRequest
	projects := map[int]*gitlab.Project{}

	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "Successful",
			fields: fields{
				APIKey: "XXXXX",
				RoomID: 12345,
			},
			args: args{
				group:         group,
				mergeRequests: mergeRequests,
				projects:      projects,
				approvals:     enum.All(),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := &ChatWorkNotifier{
				APIKey:   tt.fields.APIKey,
				RoomID:   tt.fields.RoomID,
				UserList: tt.fields.UserList,
			}
			err := n.NotifyGroupMergeRequests(tt.args.group, tt.args.mergeRequests, tt.args.projects, tt.args.approvals)
			assert.NoError(t, err)
		})
	}
}

func TestChatWorkNotifier_generateGroupMergeRequestMessage(t *testing.T) {
	type fields struct {
		APIKey   string
		RoomID   int
		UserList UserList
	}
	type args struct {
		group         *gitlab.Group
		mergeRequests []*gitlab.BasicMergeRequest
		projects      map[int]*gitlab.Project
		approvals     enum.Approvals
	}

	group := &gitlab.Group{
		FullPath: "my-group",
		WebURL:   "http://gitlab.example.com/my-group",
	}
	mergeRequests := []*gitlab.BasicMergeRequest{
		{
			Title:  "MR has assignees and reviewers",
			IID:    1,
			WebURL: "http://gitlab.example.com/my-group/my-project/merge_requests/1",
			Author: &gitlab.BasicUser{
				Username: "author",
			},
			Assignees: []*gitlab.BasicUser{
				{
					Username: "assignee1",
				},
				{
					Username: "assignee2",
				},
			},
			Reviewers: []*gitlab.BasicUser{
				{
					Username: "reviewer1",
				},
				{
					Username: "reviewer2",
				},
			},
		},
		{
			Title:  "MR no assignees and reviewers",
			IID:    2,
			WebURL: "http://gitlab.example.com/my-group/my-project/merge_requests/2",
			Author: &gitlab.BasicUser{
				Username: "author",
			},
		},
	}
	projects := map[int]*gitlab.Project{
		1: {
			PathWithNamespace: "my-group/my-project",
			WebURL:            "http://gitlab.example.com/my-group/my-project",
		},
	}

	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Has no MergeRequests",
			args: args{
				group:     group,
				projects:  projects,
				approvals: enum.All(),
			},
			want: "[info][title]No opened MergeRequests at http://gitlab.example.com/my-group[/title][/info]",
		},
		{
			name: "Has MergeRequests, All",
			args: args{
				group:         group,
				projects:      projects,
				mergeRequests: mergeRequests,
				approvals:     enum.All(),
			},
			want: `[info][title]Opened MergeRequests at http://gitlab.example.com/my-group[/title]MR has assignees and reviewers (author)
http://gitlab.example.com/my-group/my-project/merge_requests/1
Assigned to assignee1, assignee2
Reviewed to reviewer1, reviewer2[hr]MR no assignees and reviewers (author)
http://gitlab.example.com/my-group/my-project/merge_requests/2[/info]`,
		},
		{
			name: "Has MergeRequests, OnlyApproved",
			args: args{
				group:         group,
				projects:      projects,
				mergeRequests: mergeRequests,
				approvals:     enum.OnlyApproved(),
			},
			want: `[info][title]Opened MergeRequests at http://gitlab.example.com/my-group (Show only accepted MRs)[/title]MR has assignees and reviewers (author)
http://gitlab.example.com/my-group/my-project/merge_requests/1
Assigned to assignee1, assignee2
Reviewed to reviewer1, reviewer2[hr]MR no assignees and reviewers (author)
http://gitlab.example.com/my-group/my-project/merge_requests/2[/info]`,
		},
		{
			name: "Has MergeRequests, ExceptApproved",
			args: args{
				group:         group,
				projects:      projects,
				mergeRequests: mergeRequests,
				approvals:     enum.ExceptApproved(),
			},
			want: `[info][title]Opened MergeRequests at http://gitlab.example.com/my-group (Show only unaccepted MRs)[/title]MR has assignees and reviewers (author)
http://gitlab.example.com/my-group/my-project/merge_requests/1
Assigned to assignee1, assignee2
Reviewed to reviewer1, reviewer2[hr]MR no assignees and reviewers (author)
http://gitlab.example.com/my-group/my-project/merge_requests/2[/info]`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := &ChatWorkNotifier{
				APIKey:   tt.fields.APIKey,
				RoomID:   tt.fields.RoomID,
				UserList: tt.fields.UserList,
			}
			got := n.generateGroupMergeRequestMessage(tt.args.group, tt.args.mergeRequests, tt.args.projects, tt.args.approvals)
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestChatWorkNotifier_formatMergeRequest(t *testing.T) {
	type fields struct {
		APIKey   string
		RoomID   int
		UserList UserList
	}
	type args struct {
		project      *gitlab.Project
		mergeRequest *gitlab.BasicMergeRequest
	}

	project := &gitlab.Project{
		PathWithNamespace: "my-group/my-project",
		WebURL:            "http://gitlab.example.com/my-group/my-project",
	}

	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "MR no assignees and reviewers",
			args: args{
				project: project,
				mergeRequest: &gitlab.BasicMergeRequest{
					Title:  "test MR",
					IID:    1,
					WebURL: "http://gitlab.example.com/my-group/my-project/merge_requests/1",
					Author: &gitlab.BasicUser{
						Username: "author",
					},
					Assignees: []*gitlab.BasicUser{},
				},
			},
			want: `test MR (author)
http://gitlab.example.com/my-group/my-project/merge_requests/1`,
		},
		{
			name: "MR has only assignees",
			args: args{
				project: project,
				mergeRequest: &gitlab.BasicMergeRequest{
					Title:  "test MR",
					IID:    1,
					WebURL: "http://gitlab.example.com/my-group/my-project/merge_requests/1",
					Author: &gitlab.BasicUser{
						Username: "author",
					},
					Assignees: []*gitlab.BasicUser{
						{
							Username: "assignee1",
						},
						{
							Username: "assignee2",
						},
					},
					Reviewers: []*gitlab.BasicUser{},
				},
			},
			want: `test MR (author)
http://gitlab.example.com/my-group/my-project/merge_requests/1
Assigned to assignee1, assignee2`,
		},
		{
			name: "MR has only reviewers",
			args: args{
				project: project,
				mergeRequest: &gitlab.BasicMergeRequest{
					Title:  "test MR",
					IID:    1,
					WebURL: "http://gitlab.example.com/my-group/my-project/merge_requests/1",
					Author: &gitlab.BasicUser{
						Username: "author",
					},
					Assignees: []*gitlab.BasicUser{},
					Reviewers: []*gitlab.BasicUser{
						{
							Username: "reviewer1",
						},
						{
							Username: "reviewer2",
						},
					},
				},
			},
			want: `test MR (author)
http://gitlab.example.com/my-group/my-project/merge_requests/1
Reviewed to reviewer1, reviewer2`,
		},
		{
			name: "MR has only assignees and reviewers",
			args: args{
				project: project,
				mergeRequest: &gitlab.BasicMergeRequest{
					Title:  "test MR",
					IID:    1,
					WebURL: "http://gitlab.example.com/my-group/my-project/merge_requests/1",
					Author: &gitlab.BasicUser{
						Username: "author",
					},
					Assignees: []*gitlab.BasicUser{
						{
							Username: "assignee1",
						},
						{
							Username: "assignee2",
						},
					},
					Reviewers: []*gitlab.BasicUser{
						{
							Username: "reviewer1",
						},
						{
							Username: "reviewer2",
						},
					},
				},
			},
			want: `test MR (author)
http://gitlab.example.com/my-group/my-project/merge_requests/1
Assigned to assignee1, assignee2
Reviewed to reviewer1, reviewer2`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := &ChatWorkNotifier{
				APIKey:   tt.fields.APIKey,
				RoomID:   tt.fields.RoomID,
				UserList: tt.fields.UserList,
			}
			got := n.formatMergeRequest(tt.args.project, tt.args.mergeRequest)
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestChatWorkNotifier_toMention(t *testing.T) {
	type fields struct {
		APIKey   string
		RoomID   int
		UserList UserList
	}
	type args struct {
		userName string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "not included in UserList",
			fields: fields{
				UserList: UserList{
					"gitlab_user": "12345",
				},
			},
			args: args{
				userName: "root",
			},
			want: "root",
		},
		{
			name: "included in UserList",
			fields: fields{
				UserList: UserList{
					"gitlab_user": "12345",
				},
			},
			args: args{
				userName: "gitlab_user",
			},
			want: "[To:12345]gitlab_user",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := &ChatWorkNotifier{
				APIKey:   tt.fields.APIKey,
				RoomID:   tt.fields.RoomID,
				UserList: tt.fields.UserList,
			}
			got := n.toMention(tt.args.userName)
			assert.Equal(t, tt.want, got)
		})
	}
}
