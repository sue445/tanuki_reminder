package main

import (
	"fmt"
	"github.com/cockroachdb/errors"
	"gitlab.com/gitlab-org/api/client-go"
	"gitlab.com/sue445/tanuki_reminder/enum"
	"golang.org/x/sync/errgroup"
)

// Reminder represents merge request reminder
type Reminder struct {
	GitLabClientParams *GitLabClientParams
	GitLabProject      string
	GitLabGroup        string
	includeWip         bool
	MergeRequestCount  int
	Approvals          enum.Approvals
}

// Run performs get opened merge request and notify
func (r *Reminder) Run(notifier Notifier) error {
	g, err := NewGitLabClient(r.GitLabClientParams)

	if err != nil {
		return errors.WithStack(err)
	}

	if r.GitLabGroup != "" {
		return r.notifyGroupMergeRequests(notifier, g)
	}

	if r.GitLabProject != "" {
		return r.notifyProjectMergeRequests(notifier, g)
	}

	return fmt.Errorf("either project or group are required")
}

func (r *Reminder) notifyProjectMergeRequests(notifier Notifier, g *GitLab) error {
	var eg errgroup.Group

	var project *gitlab.Project
	eg.Go(func() error {
		var err error
		project, err = g.GetProject(r.GitLabProject)

		if err != nil {
			return errors.WithStack(err)
		}

		return nil
	})

	var squeezedMergeRequests []*gitlab.BasicMergeRequest
	eg.Go(func() error {
		var err error

		squeezedMergeRequests, err = getMergeRequestsWithPaging(r.MergeRequestCount, func(page int) ([]*gitlab.BasicMergeRequest, *gitlab.Response, error) {
			mergeRequests, response, err := g.ListOpenedProjectMergeRequests(r.GitLabProject, r.includeWip, r.MergeRequestCount, page)

			if err != nil {
				return nil, nil, errors.WithStack(err)
			}

			squeezedMergeRequests, err := g.SqueezeMergeRequests(mergeRequests, r.Approvals)
			if err != nil {
				return nil, nil, errors.WithStack(err)
			}

			return squeezedMergeRequests, response, nil
		})

		if err != nil {
			return errors.WithStack(err)
		}

		return nil
	})

	if err := eg.Wait(); err != nil {
		return errors.WithStack(err)
	}

	err := notifier.NotifyProjectMergeRequests(project, squeezedMergeRequests, r.Approvals)
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}

func (r *Reminder) notifyGroupMergeRequests(notifier Notifier, g *GitLab) error {
	var eg errgroup.Group

	var group *gitlab.Group
	eg.Go(func() error {
		var err error
		group, err = g.GetGroup(r.GitLabGroup)

		if err != nil {
			return errors.WithStack(err)
		}

		return nil
	})

	var squeezedMergeRequests []*gitlab.BasicMergeRequest
	eg.Go(func() error {
		var err error

		squeezedMergeRequests, err = getMergeRequestsWithPaging(r.MergeRequestCount, func(page int) ([]*gitlab.BasicMergeRequest, *gitlab.Response, error) {
			mergeRequests, response, err := g.ListOpenedGroupMergeRequests(r.GitLabGroup, r.includeWip, r.MergeRequestCount, page)

			if err != nil {
				return nil, nil, errors.WithStack(err)
			}

			squeezedMergeRequests, err := g.SqueezeMergeRequests(mergeRequests, r.Approvals)
			if err != nil {
				return nil, nil, errors.WithStack(err)
			}

			return squeezedMergeRequests, response, nil
		})

		if err != nil {
			return errors.WithStack(err)
		}

		return nil
	})

	if err := eg.Wait(); err != nil {
		return errors.WithStack(err)
	}

	projectIDs := getUniqueProjectIDs(squeezedMergeRequests)
	projects, err := g.GetProjects(projectIDs)

	if err != nil {
		return errors.WithStack(err)
	}

	err = notifier.NotifyGroupMergeRequests(group, squeezedMergeRequests, projects, r.Approvals)
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}
