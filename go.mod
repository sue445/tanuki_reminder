module gitlab.com/sue445/tanuki_reminder

go 1.24

require (
	github.com/cockroachdb/errors v1.11.3
	github.com/deckarep/golang-set v1.8.0
	github.com/griffin-stewie/go-chatwork v0.0.0-20190318122256-de7673a5e1a7
	github.com/jarcoal/httpmock v1.3.1
	github.com/slack-go/slack v0.16.0
	github.com/stretchr/testify v1.10.0
	github.com/urfave/cli v1.22.16
	gitlab.com/gitlab-org/api/client-go v0.124.0
	golang.org/x/sync v0.11.0
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/cockroachdb/logtags v0.0.0-20230118201751-21c54148d20b // indirect
	github.com/cockroachdb/redact v1.1.5 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.6 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/getsentry/sentry-go v0.27.0 // indirect
	github.com/gogo/protobuf v1.3.2 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/gorilla/websocket v1.5.1 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.7 // indirect
	github.com/kr/pretty v0.3.1 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/rogpeppe/go-internal v1.11.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	golang.org/x/net v0.33.0 // indirect
	golang.org/x/oauth2 v0.25.0 // indirect
	golang.org/x/sys v0.28.0 // indirect
	golang.org/x/text v0.21.0 // indirect
	golang.org/x/time v0.9.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
