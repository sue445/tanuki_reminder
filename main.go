package main

import (
	"fmt"
	"github.com/cockroachdb/errors"
	"github.com/urfave/cli"
	"gitlab.com/sue445/tanuki_reminder/enum"
	"log"
	"os"
	"sort"
)

var (
	// Version represents app version (injected from ldflags)
	Version string

	// Revision represents app revision (injected from ldflags)
	Revision string
)

func main() {
	app := cli.NewApp()

	app.Version = fmt.Sprintf("%s (build. %s)", Version, Revision)
	app.Name = "tanuki_reminder"
	app.Usage = "GitLab MergeRequest reminder"

	reminder := &Reminder{
		GitLabClientParams: &GitLabClientParams{},
	}
	paramApprovals := ""

	commonFlags := []cli.Flag{
		cli.StringFlag{
			Name:        "e,gitlab-api-endpoint",
			Usage:       "GitLab API Endpoint (e.g. http://gitlab.com/api/v4)",
			EnvVar:      "GITLAB_API_ENDPOINT,CI_API_V4_URL",
			Required:    true,
			Destination: &reminder.GitLabClientParams.APIEndpoint,
		},
		cli.StringFlag{
			Name:        "t,gitlab-access-token",
			Usage:       "GitLab access token",
			EnvVar:      "GITLAB_ACCESS_TOKEN",
			Required:    true,
			Destination: &reminder.GitLabClientParams.PrivateToken,
		},
		cli.StringFlag{
			Name:        "p,gitlab-project",
			Usage:       "GitLab Project Path (e.g. gitlab-org/gitlab-foss)",
			EnvVar:      "GITLAB_PROJECT,CI_PROJECT_PATH",
			Required:    false,
			Destination: &reminder.GitLabProject,
		},
		cli.StringFlag{
			Name:        "g,gitlab-group",
			Usage:       "GitLab Group Path (e.g. gitlab-org)",
			EnvVar:      "GITLAB_GROUP",
			Required:    false,
			Destination: &reminder.GitLabGroup,
		},
		cli.BoolFlag{
			Name:        "w,wip",
			Usage:       "Whether include Draft (formerly WIP) MergeRequest (default. false)",
			Required:    false,
			Destination: &reminder.includeWip,
		},
		cli.IntFlag{
			Name:        "count",
			Usage:       "MergeRequest count",
			EnvVar:      "MERGE_REQUEST_COUNT",
			Required:    false,
			Destination: &reminder.MergeRequestCount,
			Value:       10,
		},
		cli.StringFlag{
			Name:        "approvals",
			Usage:       "Squeeze by whether merge request is approved (one of 'all', 'only-approved' or 'except-approved'. default. 'all')",
			EnvVar:      "MERGE_REQUEST_APPROVALS",
			Required:    false,
			Destination: &paramApprovals,
		},
	}

	slackWebhookURL := ""
	slackChannel := ""

	chatWorkAPIKey := ""
	chatWorkRoomID := 0

	userListPath := ""

	app.Commands = []cli.Command{
		{
			Name:    "slack",
			Aliases: []string{"s"},
			Usage:   "Notify to Slack",
			Flags: append(
				commonFlags,
				cli.StringFlag{
					Name:        "u,slack-url",
					Usage:       "Slack incoming Webhook URL",
					EnvVar:      "SLACK_WEBHOOK_URL",
					Required:    true,
					Destination: &slackWebhookURL,
				},
				cli.StringFlag{
					Name:        "c,slack-channel",
					Usage:       "Slack Channel (e.g. general. default is channel in webhook)",
					EnvVar:      "SLACK_CHANNEL",
					Required:    false,
					Destination: &slackChannel,
				},
				cli.StringFlag{
					Name:        "l,user-list",
					Usage:       "User list file",
					EnvVar:      "USER_LIST",
					Required:    false,
					Destination: &userListPath,
				},
			),
			Action: func(_ *cli.Context) error {
				userList, err := LoadUserListFromFile(userListPath)

				if err != nil {
					return errors.WithStack(err)
				}

				approvals, err := parseApprovals(paramApprovals)
				if err != nil {
					return errors.WithStack(err)
				}
				reminder.Approvals = approvals

				notifier := NewSlackNotifier(slackWebhookURL, slackChannel, userList)

				return reminder.Run(notifier)
			},
		},
		{
			Name:    "chatwork",
			Aliases: []string{"c"},
			Usage:   "Notify to ChatWork",
			Flags: append(
				commonFlags,
				cli.StringFlag{
					Name:        "k,chatwork-api-key",
					Usage:       "ChatWork API Key",
					EnvVar:      "CHATWORK_API_KEY",
					Required:    true,
					Destination: &chatWorkAPIKey,
				},
				cli.IntFlag{
					Name:        "r,chatwork-room-id",
					Usage:       "ChatWork RoomID",
					EnvVar:      "CHATWORK_ROOM_ID",
					Required:    true,
					Destination: &chatWorkRoomID,
				},
				cli.StringFlag{
					Name:        "l,user-list",
					Usage:       "User list file",
					EnvVar:      "USER_LIST",
					Required:    false,
					Destination: &userListPath,
				},
			),
			Action: func(_ *cli.Context) error {
				userList, err := LoadUserListFromFile(userListPath)

				if err != nil {
					return errors.WithStack(err)
				}

				approvals, err := parseApprovals(paramApprovals)
				if err != nil {
					return errors.WithStack(err)
				}
				reminder.Approvals = approvals

				notifier := NewChatWorkNotifier(chatWorkAPIKey, chatWorkRoomID, userList)

				return reminder.Run(notifier)
			},
		},
	}

	sort.Sort(cli.CommandsByName(app.Commands))

	// Sort sub-command flags
	for _, c := range app.Commands {
		sort.Sort(cli.FlagsByName(c.Flags))
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(errors.WithStack(err))
	}
}

func parseApprovals(value string) (enum.Approvals, error) {
	approvals := enum.FindApprovals(value)
	if approvals == enum.Unknown() {
		return enum.Unknown(), fmt.Errorf("'%s' is unknown. '--approvals' must be 'all', 'only-approved' or 'except-approved'", value)
	}
	return approvals, nil
}
