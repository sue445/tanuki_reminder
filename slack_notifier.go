package main

import (
	"fmt"
	"github.com/cockroachdb/errors"
	"github.com/slack-go/slack"
	"gitlab.com/gitlab-org/api/client-go"
	"gitlab.com/sue445/tanuki_reminder/enum"
	"strings"
)

// SlackNotifier represents slack notifier
type SlackNotifier struct {
	WebhookURL string
	Channel    string
	UserList   UserList
}

// NewSlackNotifier create new SlackNotifier instance
func NewSlackNotifier(webhookURL string, channel string, userList UserList) *SlackNotifier {
	return &SlackNotifier{WebhookURL: webhookURL, Channel: channel, UserList: userList}
}

// NotifyProjectMergeRequests notifies project merge requests
func (n *SlackNotifier) NotifyProjectMergeRequests(project *gitlab.Project, mergeRequests []*gitlab.BasicMergeRequest, approvals enum.Approvals) error {
	params := &slack.WebhookMessage{
		Text: n.generateProjectMergeRequestMessage(project, mergeRequests, approvals),
	}

	if len(n.Channel) > 0 {
		params.Channel = n.Channel
	}

	err := slack.PostWebhook(n.WebhookURL, params)
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}

func (n *SlackNotifier) generateProjectMergeRequestMessage(project *gitlab.Project, mergeRequests []*gitlab.BasicMergeRequest, approvals enum.Approvals) string {
	if len(mergeRequests) == 0 {
		return fmt.Sprintf("No opened MergeRequests at <%s|%s>", project.WebURL, project.PathWithNamespace)
	}

	message := n.generateHeader(project.WebURL, project.PathWithNamespace, approvals)

	var mergeRequestMessages []string
	for _, mergeRequest := range mergeRequests {
		mergeRequestMessages = append(mergeRequestMessages, n.formatMergeRequest(project, mergeRequest))
	}

	return message + strings.Join(mergeRequestMessages, "\n\n")
}

// NotifyGroupMergeRequests notifies group merge requests
func (n *SlackNotifier) NotifyGroupMergeRequests(group *gitlab.Group, mergeRequests []*gitlab.BasicMergeRequest, projects map[int]*gitlab.Project, approvals enum.Approvals) error {
	params := &slack.WebhookMessage{
		Text: n.generateGroupMergeRequestMessage(group, mergeRequests, projects, approvals),
	}

	if len(n.Channel) > 0 {
		params.Channel = n.Channel
	}

	err := slack.PostWebhook(n.WebhookURL, params)
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}

func (n *SlackNotifier) generateGroupMergeRequestMessage(group *gitlab.Group, mergeRequests []*gitlab.BasicMergeRequest, projects map[int]*gitlab.Project, approvals enum.Approvals) string {
	if len(mergeRequests) == 0 {
		return fmt.Sprintf("No opened MergeRequests at <%s|%s>", group.WebURL, group.FullPath)
	}

	message := n.generateHeader(group.WebURL, group.FullPath, approvals)

	var mergeRequestMessages []string
	for _, mergeRequest := range mergeRequests {
		project := projects[mergeRequest.ProjectID]
		mergeRequestMessages = append(mergeRequestMessages, n.formatMergeRequest(project, mergeRequest))
	}

	return message + strings.Join(mergeRequestMessages, "\n\n")
}

func (n *SlackNotifier) formatMergeRequest(project *gitlab.Project, mergeRequest *gitlab.BasicMergeRequest) string {
	message := fmt.Sprintf(
		"[%s!%d] <%s|%s> (_%s_)",
		project.PathWithNamespace, mergeRequest.IID,
		mergeRequest.WebURL, n.sanitizeText(mergeRequest.Title),
		mergeRequest.Author.Username,
	)

	if len(mergeRequest.Assignees) > 0 {
		var userNames []string
		for _, assignee := range mergeRequest.Assignees {
			userNames = append(userNames, n.toMention(assignee.Username))
		}

		message += "\nAssigned to "
		message += strings.Join(userNames, ", ")
	}

	if len(mergeRequest.Reviewers) > 0 {
		var userNames []string
		for _, reviewer := range mergeRequest.Reviewers {
			userNames = append(userNames, n.toMention(reviewer.Username))
		}

		message += "\nReviewed to "
		message += strings.Join(userNames, ", ")
	}

	return message
}

func (n *SlackNotifier) toMention(userName string) string {
	name := n.UserList[userName]

	if name == "" {
		name = userName
	}

	return fmt.Sprintf("<@%s>", name)
}

func (n *SlackNotifier) sanitizeText(str string) string {
	// c.f. https://api.slack.com/reference/surfaces/formatting#escaping
	str = strings.ReplaceAll(str, "&", "&amp;")
	str = strings.ReplaceAll(str, "<", "&lt;")
	str = strings.ReplaceAll(str, ">", "&gt;")
	return str
}

func (n *SlackNotifier) generateHeader(url string, displayText string, approvals enum.Approvals) string {
	message := fmt.Sprintf("Opened MergeRequests at <%s|%s>", url, displayText)

	switch approvals {
	case enum.OnlyApproved():
		message += " (Show only accepted MRs)"
	case enum.ExceptApproved():
		message += " (Show only unaccepted MRs)"
	}

	return message + "\n\n"
}
