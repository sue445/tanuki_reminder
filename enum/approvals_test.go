package enum_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/sue445/tanuki_reminder/enum"
	"testing"
)

func TestFindApprovals(t *testing.T) {
	type args struct {
		value string
	}
	tests := []struct {
		name string
		args args
		want enum.Approvals
	}{
		{
			name: "all",
			args: args{
				value: "all",
			},
			want: enum.All(),
		},
		{
			name: "only-approved",
			args: args{
				value: "only-approved",
			},
			want: enum.OnlyApproved(),
		},
		{
			name: "except-approved",
			args: args{
				value: "except-approved",
			},
			want: enum.ExceptApproved(),
		},
		{
			name: "unknown value",
			args: args{
				value: "aaaa",
			},
			want: enum.Unknown(),
		},
		{
			name: "empty value",
			args: args{
				value: "",
			},
			want: enum.All(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := enum.FindApprovals(tt.args.value)

			assert.Equal(t, tt.want, got)
		})
	}
}
