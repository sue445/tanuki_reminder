package enum

// Approvals represents `--approvals` arg
type Approvals struct {
	value string
}

var unknown = Approvals{""}

var all = Approvals{"all"}

var onlyApproved = Approvals{"only-approved"}

var exceptApproved = Approvals{"except-approved"}

// Unknown represents unknown value
func Unknown() Approvals {
	return unknown
}

// All represents `--approvals=all`
func All() Approvals {
	return all
}

// OnlyApproved represents `--approvals=only-approved`
func OnlyApproved() Approvals {
	return onlyApproved
}

// ExceptApproved represents `--approvals=except-approved`
func ExceptApproved() Approvals {
	return exceptApproved
}

// Value returns Approvals value
func (a *Approvals) Value() string {
	return a.value
}

// FindApprovals returns Approvals with specified value
func FindApprovals(value string) Approvals {
	if value == "" {
		return All()
	}

	allApprovals := []Approvals{All(), OnlyApproved(), ExceptApproved()}

	for _, a := range allApprovals {
		if a.Value() == value {
			return a
		}
	}
	return Unknown()
}
