package main

import (
	"github.com/cockroachdb/errors"
	"gopkg.in/yaml.v2"
	"os"
)

// UserList represents user name mapping
type UserList map[string]string

// LoadUserListFromFile returns UserList from file
func LoadUserListFromFile(filename string) (UserList, error) {
	if filename == "" {
		return UserList{}, nil
	}

	data, err := os.ReadFile(filename)

	if err != nil {
		return nil, errors.WithStack(err)
	}

	u := UserList{}
	err = yaml.Unmarshal(data, &u)

	if err != nil {
		return nil, errors.WithStack(err)
	}

	return u, nil
}
