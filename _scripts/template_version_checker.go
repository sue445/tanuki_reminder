package main

import (
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

var currentDir string

func init() {
	var err error

	currentDir, err = os.Getwd()
	if err != nil {
		panic(err)
	}
}

func main() {
	version, err := getVersion()
	if err != nil {
		panic(err)
	}

	templateFiles := []string{
		"chatwork_reminder.yml",
		"slack_reminder.yml",
	}

	for _, templateFile := range templateFiles {
		err := checkVersionInTemplateFile(version, templateFile)
		if err != nil {
			panic(err)
		}
	}
}

func readFile(filename string) (string, error) {
	buf, err := os.ReadFile(filename)

	if err != nil {
		return "", err
	}

	return string(buf), err
}

func getVersion() (string, error) {
	body, err := readFile(filepath.Join(currentDir, "VERSION"))

	if err != nil {
		return "", err
	}

	return strings.TrimSpace(body), nil
}

func checkVersionInTemplateFile(expectedVersion string, templateFile string) error {
	templateBody, err := readFile(filepath.Join(currentDir, "ci-templates", templateFile))
	if err != nil {
		return err
	}

	matched := regexp.MustCompile(`image:\s+registry\.gitlab\.com/sue445/tanuki_reminder:([0-9.]+)`).FindAllStringSubmatch(templateBody, -1)
	if len(matched) < 1 {
		return fmt.Errorf("%s: expected 'image: registry.gitlab.com/sue445/tanuki_reminder:[0-9.]+' is contains, but actual not", templateFile)
	}

	actualVersion := matched[0][1]
	if actualVersion != expectedVersion {
		return fmt.Errorf("%s: expected actualVersion is %s, but actual is %s", templateFile, expectedVersion, actualVersion)
	}

	return nil
}
