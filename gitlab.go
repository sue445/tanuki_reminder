package main

import (
	"github.com/cockroachdb/errors"
	mapset "github.com/deckarep/golang-set"
	"gitlab.com/gitlab-org/api/client-go"
	"gitlab.com/sue445/tanuki_reminder/enum"
	"golang.org/x/sync/errgroup"
	"net/http"
	"sort"
	"sync"
)

// GitLab represents GitLab client
type GitLab struct {
	client *gitlab.Client
	params *GitLabClientParams
}

// GitLabClientParams represents parameters of NewGitLabClient
type GitLabClientParams struct {
	APIEndpoint  string
	PrivateToken string
	HTTPClient   *http.Client
}

// NewGitLabClient returns new GitLab instance
func NewGitLabClient(params *GitLabClientParams) (*GitLab, error) {
	options := []gitlab.ClientOptionFunc{gitlab.WithBaseURL(params.APIEndpoint)}
	if params.HTTPClient != nil {
		options = append(options, gitlab.WithHTTPClient(params.HTTPClient))
	}
	client, err := gitlab.NewClient(params.PrivateToken, options...)

	if err != nil {
		return nil, errors.WithStack(err)
	}

	return &GitLab{client: client, params: params}, nil
}

// GetProject returns GitLab Project
func (g *GitLab) GetProject(projectPath string) (*gitlab.Project, error) {
	project, _, err := g.client.Projects.GetProject(projectPath, &gitlab.GetProjectOptions{})

	if err != nil {
		return nil, errors.WithStack(err)
	}

	return project, nil
}

// GetProjects returns GitLab Projects with specified projectIDs
func (g *GitLab) GetProjects(projectIDs []int) (map[int]*gitlab.Project, error) {
	var eg errgroup.Group

	projects := map[int]*gitlab.Project{}
	for _, projectID := range projectIDs {
		// https://golang.org/doc/faq#closures_and_goroutines
		projectID := projectID

		eg.Go(func() error {
			project, _, err := g.client.Projects.GetProject(projectID, &gitlab.GetProjectOptions{})
			if err != nil {
				return errors.WithStack(err)
			}
			projects[projectID] = project
			return nil
		})
	}

	if err := eg.Wait(); err != nil {
		return map[int]*gitlab.Project{}, errors.WithStack(err)
	}

	return projects, nil
}

// GetGroup returns GitLab Group
func (g *GitLab) GetGroup(groupPath string) (*gitlab.Group, error) {
	group, _, err := g.client.Groups.GetGroup(groupPath, &gitlab.GetGroupOptions{})

	if err != nil {
		return nil, errors.WithStack(err)
	}

	return group, nil
}

// ListOpenedProjectMergeRequests returns opened MergeRequests in a project
func (g *GitLab) ListOpenedProjectMergeRequests(project string, includeWip bool, count int, page int) ([]*gitlab.BasicMergeRequest, *gitlab.Response, error) {
	options := &gitlab.ListProjectMergeRequestsOptions{
		State:       gitlab.Ptr("opened"),
		ListOptions: gitlab.ListOptions{PerPage: count, Page: page},
	}
	if !includeWip {
		options.WIP = gitlab.Ptr("no")
	}

	mergeRequests, response, err := g.client.MergeRequests.ListProjectMergeRequests(project, options)

	if err != nil {
		return nil, nil, errors.WithStack(err)
	}

	return mergeRequests, response, nil
}

// ListOpenedGroupMergeRequests returns opened MergeRequests in a group
func (g *GitLab) ListOpenedGroupMergeRequests(group string, includeWip bool, count int, page int) ([]*gitlab.BasicMergeRequest, *gitlab.Response, error) {
	options := &gitlab.ListGroupMergeRequestsOptions{
		State:       gitlab.Ptr("opened"),
		ListOptions: gitlab.ListOptions{PerPage: count, Page: page},
	}

	mergeRequests, response, err := g.client.MergeRequests.ListGroupMergeRequests(group, options)

	if err != nil {
		return nil, nil, errors.WithStack(err)
	}

	if includeWip {
		return mergeRequests, response, nil
	}

	var noWipMergeRequests []*gitlab.BasicMergeRequest
	for _, mr := range mergeRequests {
		if !mr.Draft {
			noWipMergeRequests = append(noWipMergeRequests, mr)
		}
	}

	return noWipMergeRequests, response, nil
}

func getUniqueProjectIDs(mergeRequests []*gitlab.BasicMergeRequest) []int {
	set := mapset.NewSet()
	for _, mr := range mergeRequests {
		set.Add(mr.ProjectID)
	}
	var projectIDs []int
	for _, id := range set.ToSlice() {
		projectIDs = append(projectIDs, id.(int))
	}

	sort.Ints(projectIDs)
	return projectIDs
}

// GetMergeRequestApprovalsConfiguration returns a request information about a merge request’s approval status
func (g *GitLab) GetMergeRequestApprovalsConfiguration(project interface{}, mrIid int) (*gitlab.MergeRequestApprovals, error) {
	config, _, err := g.client.MergeRequestApprovals.GetConfiguration(project, mrIid)

	if err != nil {
		return nil, errors.WithStack(err)
	}
	return config, nil
}

func isApproved(approved int, required int) bool {
	if required == 0 {
		return approved > 0
	}
	return approved >= required
}

// IsMergeRequestApproved returns whether merge request is approved
func (g *GitLab) IsMergeRequestApproved(project interface{}, mrIid int) (bool, error) {
	config, err := g.GetMergeRequestApprovalsConfiguration(project, mrIid)
	if err != nil {
		return false, errors.WithStack(err)
	}
	return isApproved(len(config.ApprovedBy), config.ApprovalsRequired), nil
}

// SqueezeMergeRequests returns squeezed mergeRequests with approvals
func (g *GitLab) SqueezeMergeRequests(mergeRequests []*gitlab.BasicMergeRequest, approvals enum.Approvals) ([]*gitlab.BasicMergeRequest, error) {
	if approvals == enum.All() || approvals == enum.Unknown() {
		return mergeRequests, nil
	}

	var mutex = &sync.Mutex{}
	var eg errgroup.Group
	var squeezedMergeRequests []*gitlab.BasicMergeRequest
	for _, mr := range mergeRequests {
		// https://golang.org/doc/faq#closures_and_goroutines
		mr := mr

		eg.Go(func() error {
			approved, err := g.IsMergeRequestApproved(mr.ProjectID, mr.IID)
			if err != nil {
				return errors.WithStack(err)
			}

			if (approvals == enum.OnlyApproved() && approved) || (approvals == enum.ExceptApproved() && !approved) {
				mutex.Lock()
				squeezedMergeRequests = append(squeezedMergeRequests, mr)
				mutex.Unlock()
			}

			return nil
		})
	}

	if err := eg.Wait(); err != nil {
		return []*gitlab.BasicMergeRequest{}, errors.WithStack(err)
	}

	sort.Slice(squeezedMergeRequests, func(i, j int) bool {
		// order by ProjectID DESC, IID DESC
		if squeezedMergeRequests[i].ProjectID != squeezedMergeRequests[j].ProjectID {
			return squeezedMergeRequests[i].ProjectID > squeezedMergeRequests[j].ProjectID
		}

		return squeezedMergeRequests[i].IID > squeezedMergeRequests[j].IID
	})

	return squeezedMergeRequests, nil
}

func getMergeRequestsWithPaging(count int, getMergeRequests func(page int) ([]*gitlab.BasicMergeRequest, *gitlab.Response, error)) ([]*gitlab.BasicMergeRequest, error) {
	var allMergeRequests []*gitlab.BasicMergeRequest
	page := 1

	for len(allMergeRequests) < count {
		currentMergeRequests, response, err := getMergeRequests(page)
		if err != nil {
			return []*gitlab.BasicMergeRequest{}, errors.WithStack(err)
		}
		allMergeRequests = append(allMergeRequests, currentMergeRequests...)
		if response.NextPage < 1 {
			break
		}
		page = response.NextPage
	}

	if len(allMergeRequests) <= count {
		return allMergeRequests, nil
	}

	return allMergeRequests[0:count], nil
}
