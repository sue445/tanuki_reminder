package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestLoadUserListFromFile(t *testing.T) {
	type args struct {
		filename string
	}
	tests := []struct {
		name string
		args args
		want UserList
	}{
		{
			name: "With filename",
			args: args{
				filename: "testdata/user_list/users.yml",
			},
			want: UserList{
				"user1": "foo",
				"user2": "bar",
			},
		},
		{
			name: "With empty string",
			args: args{
				filename: "",
			},
			want: UserList{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := LoadUserListFromFile(tt.args.filename)
			if assert.NoError(t, err) {
				assert.Equal(t, tt.want, got)
			}
		})
	}
}
