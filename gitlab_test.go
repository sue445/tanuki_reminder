package main

import (
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/api/client-go"
	"gitlab.com/sue445/tanuki_reminder/enum"
	"net/http"
	"os"
	"testing"
)

// ReadTestData returns testdata
func ReadTestData(filename string) string {
	buf, err := os.ReadFile(filename)

	if err != nil {
		panic(err)
	}

	return string(buf)
}

func TestGitLab_ListOpenedProjectMergeRequests(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder(
		"GET",
		"http://example.com/api/v4/",
		func(_ *http.Request) (*http.Response, error) {
			resp := httpmock.NewStringResponse(404, "{\"error\":\"404 Not Found\"}")
			resp.Header.Set("RateLimit-Limit", "600")
			return resp, nil
		},
	)
	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/projects/my-group%2Fmy-project/merge_requests?page=1&per_page=100&state=opened",
		httpmock.NewStringResponder(200, ReadTestData("testdata/gitlab/list_project_merge_requests_with_wip.json")))
	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/projects/my-group%2Fmy-project/merge_requests?page=1&per_page=100&state=opened&wip=no",
		httpmock.NewStringResponder(200, ReadTestData("testdata/gitlab/list_project_merge_requests_without_wip.json")))

	params := &GitLabClientParams{
		APIEndpoint:  "https://gitlab.example.com/api/v4",
		PrivateToken: "XXXXXX",
		HTTPClient:   http.DefaultClient,
	}
	g, err := NewGitLabClient(params)

	require.NoError(t, err)

	type args struct {
		project    string
		includeWip bool
		count      int
		page       int
	}
	tests := []struct {
		name       string
		args       args
		wantTitles []string
		wantErr    bool
	}{
		{
			name: "with WIP",
			args: args{
				project:    "my-group/my-project",
				includeWip: true,
				count:      100,
				page:       1,
			},
			wantTitles: []string{"test1", "WIP: test2"},
		},
		{
			name: "without WIP",
			args: args{
				project:    "my-group/my-project",
				includeWip: false,
				count:      100,
				page:       1,
			},
			wantTitles: []string{"test1"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, _, err := g.ListOpenedProjectMergeRequests(tt.args.project, tt.args.includeWip, tt.args.count, tt.args.page)

			if tt.wantErr {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
			}

			var gotTitles []string
			for _, mr := range got {
				gotTitles = append(gotTitles, mr.Title)
			}
			assert.Equal(t, tt.wantTitles, gotTitles)
		})
	}
}

func TestGitLab_ListOpenedGroupMergeRequests(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder(
		"GET",
		"http://example.com/api/v4/",
		func(_ *http.Request) (*http.Response, error) {
			resp := httpmock.NewStringResponse(404, "{\"error\":\"404 Not Found\"}")
			resp.Header.Set("RateLimit-Limit", "600")
			return resp, nil
		},
	)
	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/groups/my-group/merge_requests?page=1&per_page=100&state=opened",
		httpmock.NewStringResponder(200, ReadTestData("testdata/gitlab/list_group_merge_requests.json")))

	params := &GitLabClientParams{
		APIEndpoint:  "https://gitlab.example.com/api/v4",
		PrivateToken: "XXXXXX",
		HTTPClient:   http.DefaultClient,
	}
	g, err := NewGitLabClient(params)

	require.NoError(t, err)

	type args struct {
		group      string
		includeWip bool
		count      int
		page       int
	}
	tests := []struct {
		name       string
		args       args
		wantTitles []string
	}{
		{
			name: "with WIP",
			args: args{
				group:      "my-group",
				includeWip: true,
				count:      100,
				page:       1,
			},
			wantTitles: []string{"test1", "WIP: test2"},
		},
		{
			name: "without WIP",
			args: args{
				group:      "my-group",
				includeWip: false,
				count:      100,
				page:       1,
			},
			wantTitles: []string{"test1"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, _, err := g.ListOpenedGroupMergeRequests(tt.args.group, tt.args.includeWip, tt.args.count, tt.args.page)

			if assert.NoError(t, err) {
				var gotTitles []string
				for _, mr := range got {
					gotTitles = append(gotTitles, mr.Title)
				}
				assert.Equal(t, tt.wantTitles, gotTitles)
			}
		})
	}
}

func TestGitLab_GetProjects(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/projects/3",
		httpmock.NewStringResponder(200, ReadTestData("testdata/gitlab/get_single_project.json")))

	params := &GitLabClientParams{
		APIEndpoint:  "https://gitlab.example.com/api/v4",
		PrivateToken: "XXXXXX",
		HTTPClient:   http.DefaultClient,
	}
	g, err := NewGitLabClient(params)

	require.NoError(t, err)

	projects, err := g.GetProjects([]int{3})
	require.NoError(t, err)

	project := projects[3]
	if assert.NotNil(t, project, "Not Found projectID with 3") {
		assert.Equal(t, "Diaspora Project Site", project.Name)
	}
}

func Test_getUniqueProjectIDs(t *testing.T) {
	mergeRequests := []*gitlab.BasicMergeRequest{
		{
			ProjectID: 1,
		},
		{
			ProjectID: 3,
		},
		{
			ProjectID: 2,
		},
		{
			ProjectID: 1,
		},
	}

	projectIDs := getUniqueProjectIDs(mergeRequests)
	assert.Equal(t, []int{1, 2, 3}, projectIDs)
}

func TestGitLab_GetMergeRequestApprovalsConfiguration(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/projects/my-group%2Fmy-project/merge_requests/5/approvals",
		httpmock.NewStringResponder(200, ReadTestData("testdata/gitlab/get_merge_request_approvals_configuration_contains_not_approved.json")))

	params := &GitLabClientParams{
		APIEndpoint:  "https://gitlab.example.com/api/v4",
		PrivateToken: "XXXXXX",
		HTTPClient:   http.DefaultClient,
	}
	g, err := NewGitLabClient(params)

	require.NoError(t, err)

	got, err := g.GetMergeRequestApprovalsConfiguration("my-group/my-project", 5)
	if assert.NoError(t, err) {
		assert.Equal(t, 2, got.ApprovalsRequired)
		assert.Equal(t, 1, got.ApprovalsLeft)
	}
}

func Test_isApproved(t *testing.T) {
	type args struct {
		approved int
		required int
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "approved > required",
			args: args{
				approved: 2,
				required: 1,
			},
			want: true,
		},
		{
			name: "approved >= required",
			args: args{
				approved: 1,
				required: 1,
			},
			want: true,
		},
		{
			name: "approved < required",
			args: args{
				approved: 0,
				required: 1,
			},
			want: false,
		},
		{
			name: "approved >= required && required == 0",
			args: args{
				approved: 0,
				required: 0,
			},
			want: false,
		},
		{
			name: "approved > required && required == 0",
			args: args{
				approved: 1,
				required: 0,
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := isApproved(tt.args.approved, tt.args.required)
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestGitLab_IsMergeRequestApproved(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/projects/my-group%2Fmy-project/merge_requests/5/approvals",
		httpmock.NewStringResponder(200, ReadTestData("testdata/gitlab/get_merge_request_approvals_configuration_contains_not_approved.json")))

	params := &GitLabClientParams{
		APIEndpoint:  "https://gitlab.example.com/api/v4",
		PrivateToken: "XXXXXX",
		HTTPClient:   http.DefaultClient,
	}

	g, err := NewGitLabClient(params)
	require.NoError(t, err)

	type args struct {
		project string
		mrIid   int
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "successful",
			args: args{
				project: "my-group/my-project",
				mrIid:   5,
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := g.IsMergeRequestApproved(tt.args.project, tt.args.mrIid)
			if assert.NoError(t, err) {
				assert.Equal(t, tt.want, got)
			}
		})
	}
}

func TestGitLab_SqueezeMergeRequests(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/projects/1/merge_requests/1/approvals",
		httpmock.NewStringResponder(200, ReadTestData("testdata/gitlab/get_merge_request_approvals_configuration_contains_approved.json")))
	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/projects/1/merge_requests/2/approvals",
		httpmock.NewStringResponder(200, ReadTestData("testdata/gitlab/get_merge_request_approvals_configuration_contains_approved.json")))
	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/projects/2/merge_requests/1/approvals",
		httpmock.NewStringResponder(200, ReadTestData("testdata/gitlab/get_merge_request_approvals_configuration_contains_approved.json")))

	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/projects/1/merge_requests/11/approvals",
		httpmock.NewStringResponder(200, ReadTestData("testdata/gitlab/get_merge_request_approvals_configuration_contains_not_approved.json")))
	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/projects/1/merge_requests/12/approvals",
		httpmock.NewStringResponder(200, ReadTestData("testdata/gitlab/get_merge_request_approvals_configuration_contains_not_approved.json")))
	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/projects/2/merge_requests/11/approvals",
		httpmock.NewStringResponder(200, ReadTestData("testdata/gitlab/get_merge_request_approvals_configuration_contains_not_approved.json")))

	approvedMR1 := &gitlab.BasicMergeRequest{
		ProjectID: 1,
		IID:       1,
		Title:     "Approved MR 1",
	}
	approvedMR2 := &gitlab.BasicMergeRequest{
		ProjectID: 1,
		IID:       2,
		Title:     "Approved MR 2",
	}
	approvedMR3 := &gitlab.BasicMergeRequest{
		ProjectID: 2,
		IID:       1,
		Title:     "Approved MR 3",
	}

	notApprovedMR1 := &gitlab.BasicMergeRequest{
		ProjectID: 1,
		IID:       11,
		Title:     "Not approved MR 1",
	}

	notApprovedMR2 := &gitlab.BasicMergeRequest{
		ProjectID: 1,
		IID:       12,
		Title:     "Not approved MR 2",
	}

	notApprovedMR3 := &gitlab.BasicMergeRequest{
		ProjectID: 2,
		IID:       11,
		Title:     "Not approved MR 3",
	}

	mergeRequests := []*gitlab.BasicMergeRequest{
		approvedMR3,
		approvedMR2,
		approvedMR1,
		notApprovedMR3,
		notApprovedMR2,
		notApprovedMR1,
	}

	params := &GitLabClientParams{
		APIEndpoint:  "https://gitlab.example.com/api/v4",
		PrivateToken: "XXXXXX",
		HTTPClient:   http.DefaultClient,
	}
	g, err := NewGitLabClient(params)
	require.NoError(t, err)

	type args struct {
		mergeRequests []*gitlab.BasicMergeRequest
		approvals     enum.Approvals
	}
	tests := []struct {
		name string
		args args
		want []*gitlab.BasicMergeRequest
	}{
		{
			name: "with ALL",
			args: args{
				mergeRequests: mergeRequests,
				approvals:     enum.All(),
			},
			want: mergeRequests,
		},
		{
			name: "with OnlyApproved",
			args: args{
				mergeRequests: mergeRequests,
				approvals:     enum.OnlyApproved(),
			},
			want: []*gitlab.BasicMergeRequest{approvedMR3, approvedMR2, approvedMR1},
		},
		{
			name: "with ExceptApproved",
			args: args{
				mergeRequests: mergeRequests,
				approvals:     enum.ExceptApproved(),
			},
			want: []*gitlab.BasicMergeRequest{notApprovedMR3, notApprovedMR2, notApprovedMR1},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := g.SqueezeMergeRequests(tt.args.mergeRequests, tt.args.approvals)
			if assert.NoError(t, err) {
				assert.Equal(t, tt.want, got)
			}
		})
	}
}

func Test_getMergeRequestsWithPaging(t *testing.T) {
	mr1 := &gitlab.BasicMergeRequest{
		ProjectID: 1,
		IID:       1,
		Title:     "MR 1",
	}
	mr2 := &gitlab.BasicMergeRequest{
		ProjectID: 1,
		IID:       2,
		Title:     "MR 2",
	}
	mr3 := &gitlab.BasicMergeRequest{
		ProjectID: 1,
		IID:       3,
		Title:     "MR 3",
	}

	getMergeRequests := func(_ int) ([]*gitlab.BasicMergeRequest, *gitlab.Response, error) {
		mergeRequests := []*gitlab.BasicMergeRequest{mr1, mr2, mr3}
		response := &gitlab.Response{NextPage: 0}
		return mergeRequests, response, nil
	}

	type args struct {
		count            int
		getMergeRequests func(page int) ([]*gitlab.BasicMergeRequest, *gitlab.Response, error)
	}
	tests := []struct {
		name string
		args args
		want []*gitlab.BasicMergeRequest
	}{
		{
			name: "len(mergeRequests) == count",
			args: args{
				count:            3,
				getMergeRequests: getMergeRequests,
			},
			want: []*gitlab.BasicMergeRequest{mr1, mr2, mr3},
		},
		{
			name: "len(mergeRequests) < count",
			args: args{
				count:            4,
				getMergeRequests: getMergeRequests,
			},
			want: []*gitlab.BasicMergeRequest{mr1, mr2, mr3},
		},
		{
			name: "len(mergeRequests) > count",
			args: args{
				count:            2,
				getMergeRequests: getMergeRequests,
			},
			want: []*gitlab.BasicMergeRequest{mr1, mr2},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := getMergeRequestsWithPaging(tt.args.count, tt.args.getMergeRequests)
			if assert.NoError(t, err) {
				assert.Equal(t, tt.want, got)
			}
		})
	}
}
